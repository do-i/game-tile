package com.djd.fun.gametile;

import java.awt.BorderLayout;
import java.awt.LayoutManager;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.djd.fun.gametile.annotation.AccessibleTileIndex;
import com.djd.fun.gametile.annotation.Charas;
import com.djd.fun.gametile.annotation.Ground;
import com.djd.fun.gametile.annotation.GroundTileSize;
import com.djd.fun.gametile.annotation.LayoutBorder;
import com.djd.fun.gametile.annotation.NonPlayerCharacter;
import com.djd.fun.gametile.annotation.PlayerCharacter;
import com.djd.fun.gametile.annotation.Terrain;
import com.djd.fun.gametile.annotation.TileSize16x16;
import com.djd.fun.gametile.annotation.TileSize32x32;
import com.djd.fun.gametile.gui.GamePanel;
import com.djd.fun.gametile.gui.MainFrame;
import com.djd.fun.gametile.gui.MainPanel;
import com.djd.fun.gametile.gui.TilesFactory;
import com.djd.fun.gametile.gui.model.ImmutableSize;
import com.djd.fun.gametile.gui.model.Tiles;
import com.djd.fun.gametile.model.CharacterState;
import com.djd.fun.gametile.model.CharacterStateFactory;
import com.djd.fun.gametile.model.Dimension;
import com.djd.fun.gametile.model.Location2D;
import com.djd.fun.gametile.model.SpriteIndex;
import com.djd.fun.gametile.model.WeaponFactory;
import com.djd.fun.gametile.service.Location2DService;
import com.djd.fun.gametile.service.Location2DServiceImpl;
import com.djd.fun.gametile.service.ManhattanMovement;
import com.djd.fun.gametile.service.MovementService;
import com.djd.fun.gametile.tiled.TiledDataModule;
import com.djd.fun.gametile.tiled.TiledDataResolver;
import com.djd.fun.io.MoreResources;
import com.google.common.collect.ImmutableSet;
import com.google.inject.PrivateModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.djd.fun.gametile.model.Direction.EAST;
import static com.djd.fun.gametile.model.Direction.NORTH;
import static com.djd.fun.gametile.model.Direction.SOUTH;
import static com.djd.fun.gametile.model.Direction.WEST;
import static com.djd.fun.gametile.model.Posture.CENTER;
import static com.djd.fun.gametile.model.Posture.LEFT;
import static com.djd.fun.gametile.model.Posture.RIGHT;

public class GameModule extends PrivateModule {

  private static final Logger log = LoggerFactory.getLogger(GameModule.class);

  @Override
  protected void configure() {
    Names.bindProperties(binder(), System.getProperties());
    install(new FactoryModuleBuilder().build(TilesFactory.class));
    install(new FactoryModuleBuilder().build(CharacterStateFactory.class));
    install(new FactoryModuleBuilder().build(WeaponFactory.class));
    install(new TiledDataModule());
    bind(JPanel.class).to(MainPanel.class).in(Singleton.class);
    bind(JPanel.class).annotatedWith(Ground.class).to(GamePanel.class).in(Singleton.class);
    bind(JFrame.class).to(MainFrame.class);
    bind(Location2DService.class).to(Location2DServiceImpl.class).in(Singleton.class);
    bind(MovementService.class).to(ManhattanMovement.class).in(Singleton.class);
    expose(JFrame.class);
  }

  @Provides @Singleton @LayoutBorder
  LayoutManager provideBorderLayout() {
    log.info("provideBorderLayout");
    return new BorderLayout();
  }

  @Provides @Singleton @PlayerCharacter
  CharacterState provideCharacterState(CharacterStateFactory characterStateFactory) {
    return characterStateFactory.createCharacter(Location2D.of(2, 1));
  }

  @Provides @Singleton @AccessibleTileIndex
  ImmutableSet<Integer> provideAccessibleTileIndex() {
    return ImmutableSet.of(13, 65, 66, 89, 90);
  }

  @Provides @Singleton @PlayerCharacter
  SpriteIndex provideSpriteIndexForPlayerChara() {
    return SpriteIndex.builder()
        .put(NORTH, LEFT, 42)
        .put(NORTH, CENTER, 41)
        .put(NORTH, RIGHT, 40)
        .put(EAST, LEFT, 30)
        .put(EAST, CENTER, 29)
        .put(EAST, RIGHT, 28)
        .put(SOUTH, LEFT, 6)
        .put(SOUTH, CENTER, 5)
        .put(SOUTH, RIGHT, 4)
        .put(WEST, LEFT, 18)
        .put(WEST, CENTER, 17)
        .put(WEST, RIGHT, 16)
        .build();
  }

  @Provides @Singleton @NonPlayerCharacter
  SpriteIndex provideSpriteIndexForNonPlayerChara() {
    return SpriteIndex.builder()
        .put(NORTH, LEFT, 91)
        .put(NORTH, CENTER, 92)
        .put(NORTH, RIGHT, 93)
        .put(EAST, LEFT, 79)
        .put(EAST, CENTER, 80)
        .put(EAST, RIGHT, 81)
        .put(SOUTH, LEFT, 55)
        .put(SOUTH, CENTER, 56)
        .put(SOUTH, RIGHT, 57)
        .put(WEST, LEFT, 67)
        .put(WEST, CENTER, 68)
        .put(WEST, RIGHT, 69)
        .build();
  }

  @Provides @Singleton @Ground
  ImmutableSize provideImmutableSize() {
    return ImmutableSize.of(640, 640);
  }

  @Provides @Singleton @Terrain
  Tiles provideTerrainTiles(TilesFactory tilesFactory, @TileSize16x16 ImmutableSize tileSize) {
    return tilesFactory.loadTiles(MoreResources.getResource("basictiles.png"), tileSize);
  }

  @Provides @Singleton @Charas
  Tiles provideCharacterTiles(TilesFactory tilesFactory, @TileSize16x16 ImmutableSize tileSize) {
    return tilesFactory.loadTiles(MoreResources.getResource("characters.png"), tileSize);
  }

  @Provides @Singleton
  Dimension provideDimension(TiledDataResolver tiledDataResolver) {
    return Dimension.of(tiledDataResolver.getMapWidth(), tiledDataResolver.getMapHeight());
  }

  @Provides @Singleton @NonPlayerCharacter
  int provideAnimationDelay() {
    return 500;
  }

  @Provides @Singleton @TileSize16x16
  ImmutableSize provide16x16TileSize() {
    return ImmutableSize.of(16, 16);
  }

  @Provides @Singleton @TileSize32x32
  ImmutableSize provide32x32TileSize() {
    return ImmutableSize.of(32, 32);
  }

  @Deprecated
  @Provides @Singleton @GroundTileSize
  ImmutableSize provideGroundTileSize() {
    return ImmutableSize.of(32, 32);
  }
}
