package com.djd.fun.gametile;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {

  public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> {
      Injector injector = Guice.createInjector(new GameModule());
      JFrame jFrame = injector.getInstance(JFrame.class);
      jFrame.pack();
      jFrame.setVisible(true);
    });
  }
}
