package com.djd.fun.gametile.gui;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import com.djd.fun.gametile.annotation.Charas;
import com.djd.fun.gametile.annotation.NonPlayerCharacter;
import com.djd.fun.gametile.annotation.PlayerCharacter;
import com.djd.fun.gametile.annotation.Terrain;
import com.djd.fun.gametile.annotation.TileSize32x32;
import com.djd.fun.gametile.gui.model.ImmutableSize;
import com.djd.fun.gametile.gui.model.Tile;
import com.djd.fun.gametile.gui.model.Tiles;
import com.djd.fun.gametile.model.CharacterState;
import com.djd.fun.gametile.model.CharacterStateFactory;
import com.djd.fun.gametile.model.Location2D;
import com.djd.fun.gametile.model.SpriteIndex;
import com.djd.fun.gametile.model.WeaponFactory;
import com.djd.fun.gametile.model.WeaponState;
import com.djd.fun.gametile.service.MovementService;
import com.djd.fun.gametile.tiled.TiledDataResolver;
import com.djd.fun.gametile.tiled.model.TiledDataRequest;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimaps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.djd.fun.gametile.model.Direction.EAST;
import static com.djd.fun.gametile.model.Direction.NORTH;
import static com.djd.fun.gametile.model.Direction.SOUTH;
import static com.djd.fun.gametile.model.Direction.WEST;
import static com.djd.fun.gametile.tiled.model.TiledDataRequest.LAYER_5_TREASURE_FG;
import static com.djd.fun.gametile.tiled.model.TiledDataRequest.LAYER_6_TOP;
import static java.awt.event.KeyEvent.VK_DOWN;
import static java.awt.event.KeyEvent.VK_LEFT;
import static java.awt.event.KeyEvent.VK_RIGHT;
import static java.awt.event.KeyEvent.VK_SPACE;
import static java.awt.event.KeyEvent.VK_UP;
import static javax.swing.JOptionPane.YES_OPTION;

/**
 * TODO separate painter and else
 * Main game panel orchestrate player character movement, NPC movement, game status (win or lose)
 */
public class GamePanel extends Abstract2DPanel {

  private static final Logger log = LoggerFactory.getLogger(GamePanel.class);
  private final ImmutableSize renderTileSize;
  private final Tiles charaTiles;
  private final TiledDataResolver tiledDataResolver;
  private final MovementService movementService;
  private final SpriteIndex spriteIndexPC;
  private final SpriteIndex spriteIndexNPC;
  private final WeaponFactory weaponFactory;
  private final Timer npcTimer;
  private final ActionListener nonPlayerCharacterAnimator;
  private final ImmutableList<Location2D> gridLocations;

  /*
  Keep key listener reference so that when game ends, we can remove the listener from the panel.
   */
  private final KeyListener keyListener;
  /*
  mutable objects
   */
  private final CharacterState playerCharacterState;
  private final Location2D treasureLocation;
  private final ImmutableSet<CharacterState> nonPlayerCharacterState;

  /*
  mutable object reference
  */
  private List<WeaponState> weaponStates;
  private int level = 1;

  @Inject
  public GamePanel(@TileSize32x32 ImmutableSize renderTileSize,
      @Terrain Tiles terrainTiles,
      @Charas Tiles charaTiles,
      TiledDataResolver tiledDataResolver,
      MovementService movementService,
      @PlayerCharacter CharacterState playerCharacterState,
      @PlayerCharacter SpriteIndex spriteIndexPC,
      @NonPlayerCharacter SpriteIndex spriteIndexNPC,
      @NonPlayerCharacter int animationDelay,
      CharacterStateFactory characterStateFactory,
      WeaponFactory weaponFactory) {
    this.renderTileSize = renderTileSize;
    this.charaTiles = charaTiles;
    this.tiledDataResolver = tiledDataResolver;
    this.movementService = movementService;
    this.spriteIndexPC = spriteIndexPC;
    this.spriteIndexNPC = spriteIndexNPC;
    this.weaponFactory = weaponFactory;
    this.playerCharacterState = playerCharacterState;
    this.gridLocations = createGridLocations(tiledDataResolver.getMapWidth(), tiledDataResolver.getMapHeight());
    this.treasureLocation = createTreasureLocation(tiledDataResolver);
    this.nonPlayerCharacterState = createNonPlayerCharacters(characterStateFactory);
    this.nonPlayerCharacterAnimator = new NonPlayerCharacterAnimator();
    this.keyListener = new KeyInputListener();
    this.npcTimer = new Timer(animationDelay, nonPlayerCharacterAnimator);
    init();
  }

  private static ImmutableList<Location2D> createGridLocations(int width, int height) {
    ImmutableList.Builder<Location2D> locations = ImmutableList.builder();
    for (int row = 0; row < height; row++) {
      for (int col = 0; col < width; col++) {
        locations.add(Location2D.of(row, col));
      }
    }
    return locations.build();
  }

  private void init() {
    weaponStates = new ArrayList<>();
    playerCharacterState.init();
    nonPlayerCharacterState.forEach(CharacterState::init);
    addKeyListener(keyListener);
    setFocusable(true); // this is for keyListener
    npcTimer.start();
  }

  /**
   * TODO convert this static method to factory pattern with @Assisted int level and inject TiledDataResolver
   *
   * @param tiledDataResolver
   * @return {@link Location2D} location of treasure
   */
  private static Location2D createTreasureLocation(TiledDataResolver tiledDataResolver) {
    return tiledDataResolver.findTreasureLocation(TiledDataRequest.builder()
        .layerName(LAYER_5_TREASURE_FG)
        .build());
  }

  private static ImmutableSet<CharacterState> createNonPlayerCharacters(CharacterStateFactory characterStateFactory) {
    // TODO load npc location from npc_locations.csv ==> 0 or 1 matrix
    return ImmutableSet.of(
        Location2D.of(18, 18),
        Location2D.of(18, 7),
        Location2D.of(7, 18)).stream()
        .map(characterStateFactory::createCharacter)
        .collect(ImmutableSet.toImmutableSet());
  }

  @Override
  protected void paintComponent(Graphics2D g) {
    paintTerrain(g);
    paintTreasure(g);
    paintPlayer(g);
    paintEnemy(g);
    paintWeapon(g);
    paintTop(g);
  }

  private void paintTerrain(Graphics2D g) {
    gridLocations.parallelStream().forEach(location ->
        TiledDataRequest.getTerrainLayerNames().stream()
            .map(layerName -> TiledDataRequest.builder()
                .location(location)
                .level(level)
                .layerName(layerName)
                .build())
            .map(tiledDataResolver::getSprite)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .forEachOrdered(sprite -> drawSprite(g, sprite, location)));
  }


  private void paintTreasure(Graphics2D g) {
    tiledDataResolver.getSprite(TiledDataRequest.builder()
        .location(treasureLocation)
        .level(level)
        .layerName(LAYER_5_TREASURE_FG)
        .build()).ifPresent(sprite -> drawSprite(g, sprite, treasureLocation));
  }

  private void paintPlayer(Graphics2D g) {
    Tile tile = charaTiles.getTile(
        spriteIndexPC.getIndex(playerCharacterState.getActiveDirection(), playerCharacterState.getActivePosture()));
    drawSprite(g, tile.getSprite(), playerCharacterState.getActiveLocation());
  }

  private void paintEnemy(Graphics2D g) {
    nonPlayerCharacterState.stream()
        .filter(CharacterState::isActive)
        .forEach(characterState -> {
          Tile tile = charaTiles.getTile(spriteIndexNPC.getIndex(
              characterState.getActiveDirection(), characterState.getActivePosture()));
          drawSprite(g, tile.getSprite(), characterState.getActiveLocation());
        });
  }

  private void paintWeapon(Graphics2D g) {
    weaponStates.stream()
        .filter(WeaponState::isActive)
        .forEach(weaponState -> {
          Tile tile = charaTiles.getTile(65);
          drawSprite(g, tile.getSprite(), weaponState.getActiveLocation());
        });
  }

  private void paintTop(Graphics2D g) {
    gridLocations.parallelStream().forEach(location ->
        tiledDataResolver.getSprite(TiledDataRequest.builder()
            .location(location)
            .level(level)
            .layerName(LAYER_6_TOP)
            .build()).ifPresent(sprite -> drawSprite(g, sprite, location)));
  }

  private void drawSprite(Graphics2D g, BufferedImage sprite, Location2D location) {
    g.drawImage(sprite,
        location.getColIndex() * renderTileSize.getWidth(),
        location.getRowIndex() * renderTileSize.getHeight(),
        renderTileSize.getWidth(),
        renderTileSize.getHeight(),
        this);
  }

  private void checkGameStatus() {
    Location2D playerActiveLocation = playerCharacterState.getActiveLocation();
    boolean isPlayerDead = nonPlayerCharacterState.stream()
        .filter(CharacterState::isActive)
        .map(CharacterState::getActiveLocation)
        .anyMatch(playerActiveLocation::equals);
    if (isPlayerDead) {
      npcTimer.stop();
      removeKeyListener(keyListener);
      showDialog("Mission Failed");
    } else if (treasureLocation.equals(playerActiveLocation)) {
      npcTimer.stop();
      removeKeyListener(keyListener);
      showDialog("Mission Accomplished");
    } else {
      /*
      This part is to destroy npc with weapon. If weapon and npc(s) are in a same location, disable all active npc(s)
       */
      ImmutableListMultimap<Location2D, CharacterState> npcStateByLocation =
          Multimaps.index(nonPlayerCharacterState, CharacterState::getActiveLocation);
      for (WeaponState weaponState : weaponStates) {
        if (weaponState.isActive()) {
          ImmutableList<CharacterState> npcStates = npcStateByLocation.get(weaponState.getActiveLocation());
          npcStates.stream()
              .filter(CharacterState::isActive)
              .forEach(npcState -> {
                npcState.deactivate();
                weaponState.deactivate();
              });
        }
      }
    }
  }

  private void showDialog(String message) {
    if (JOptionPane.showConfirmDialog(this, message + " Reinitialize the Board?") == YES_OPTION) {
      init();
    }
  }

  private void attack() {
    weaponStates.add(weaponFactory.createWeaponLocation(
        playerCharacterState.getActiveLocation(), playerCharacterState.getActiveDirection()));
    log.info("weapon fired {}", weaponStates);
  }

  private class KeyInputListener extends KeyAdapter {

    @Override
    public void keyPressed(KeyEvent event) {
      switch (event.getKeyCode()) {
        case VK_UP:
          playerCharacterState.moveTowards(NORTH);
          break;
        case VK_RIGHT:
          playerCharacterState.moveTowards(EAST);
          break;
        case VK_DOWN:
          playerCharacterState.moveTowards(SOUTH);
          break;
        case VK_LEFT:
          playerCharacterState.moveTowards(WEST);
          break;
        case VK_SPACE:
          attack();
          break;
        default:
          log.info("{} keycode was pressed", event.getKeyCode());
      }
      repaint();
      checkGameStatus();
    }
  }

  private class NonPlayerCharacterAnimator implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent event) {
      for (CharacterState npcState : nonPlayerCharacterState) {
        // TODO create service method to return other npc locations.
        // TODO add terrain inaccessible locations.
        ImmutableSet<Location2D> obstacles = ImmutableSet.<Location2D>builder()
            .addAll(nonPlayerCharacterState.stream()
                .filter(CharacterState::isActive)
                .map(CharacterState::getActiveLocation)
                .collect(Collectors.toSet()))
            .build();
        // TODO update npcState with new activeLocation
        Location2D nextLocation = movementService.computeNextMove(npcState.getActiveLocation(),
            playerCharacterState.getActiveLocation(), obstacles);
        npcState.moveTo(nextLocation);
      }
      for (WeaponState weaponState : weaponStates) {
        weaponState.move();
      }
      repaint();
      checkGameStatus();
    }
  }
}
