package com.djd.fun.gametile.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import com.djd.fun.gametile.gui.util.Fonts;
import com.google.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainFrame extends JFrame {

  private static final Logger log = LoggerFactory.getLogger(MainFrame.class);
  private static final String FRAME_TITLE = "Koala";

  @Inject
  public MainFrame(JPanel mainPanel, MenuBar menuBar) {
    super(FRAME_TITLE);
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setJMenuBar(menuBar);
    add(mainPanel);
    Fonts.configure();
    log.info("init JFrame");
  }
}
