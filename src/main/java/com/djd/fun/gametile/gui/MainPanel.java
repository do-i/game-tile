package com.djd.fun.gametile.gui;

import java.awt.Dimension;
import java.awt.LayoutManager;

import javax.swing.JPanel;

import com.djd.fun.gametile.annotation.Ground;
import com.djd.fun.gametile.annotation.LayoutBorder;
import com.djd.fun.gametile.gui.model.ImmutableSize;
import com.google.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainPanel extends JPanel {

  private static final Logger log = LoggerFactory.getLogger(MainPanel.class);
  private final ImmutableSize panelSize;

  @Inject
  public MainPanel(@LayoutBorder LayoutManager layout, @Ground JPanel jPane, @Ground ImmutableSize panelSize) {
    this.panelSize = panelSize;
    setLayout(layout);
    add(jPane);
    log.info("init");
  }

  @Override
  public Dimension getPreferredSize() {
    return panelSize.toDimension();
  }
}
