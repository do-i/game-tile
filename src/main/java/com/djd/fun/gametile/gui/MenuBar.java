package com.djd.fun.gametile.gui;

import java.awt.Font;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;

public class MenuBar extends JMenuBar {

  public MenuBar() {
    Font f = new Font("sans-serif", Font.PLAIN, 12);
    UIManager.put("Menu.font", f);
    UIManager.put("MenuItem.font", f);
    super.add(createFileMenu());
  }

  private static JMenu createFileMenu() {
    JMenu jMenuFile = new JMenu("Game");
    JMenuItem jMenuItemExit = new JMenuItem("Exit");
    jMenuItemExit.addActionListener(event -> System.exit(0));
    jMenuFile.add(jMenuItemExit);
    return jMenuFile;
  }
}
