package com.djd.fun.gametile.gui;

import java.net.URL;

import com.djd.fun.gametile.gui.model.ImmutableSize;
import com.djd.fun.gametile.gui.model.Tiles;

public interface TilesFactory {
  Tiles loadTiles(URL tilesFileURL, ImmutableSize tileSize);
}
