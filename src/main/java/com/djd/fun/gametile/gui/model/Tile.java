package com.djd.fun.gametile.gui.model;

import java.awt.image.BufferedImage;
import java.util.Objects;

import com.google.common.base.MoreObjects;

/**
 * A representation of a tile which comprised of sprite (aka image) and attributes such as mobility, accessibility, etc.
 */
public class Tile {
  private final String name;
  private final boolean accessible;
  private final BufferedImage sprite;

  public Tile(String name, boolean accessible, BufferedImage sprite) {
    this.name = name;
    this.accessible = accessible;
    this.sprite = sprite;
  }

  public String getName() {
    return name;
  }

  public boolean isAccessible() {
    return accessible;
  }

  public BufferedImage getSprite() {
    return sprite;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Tile tile = (Tile)o;
    return accessible == tile.accessible &&
        Objects.equals(name, tile.name);
  }

  @Override public int hashCode() {
    return Objects.hash(name, accessible);
  }

  @Override public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("name", name)
        .add("accessible", accessible)
        .toString();
  }
}
