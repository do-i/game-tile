package com.djd.fun.gametile.gui.model;

import com.djd.fun.gametile.model.Location2D;
import com.google.common.annotations.VisibleForTesting;

public class TileUtil {

  // no instance
  private TileUtil(){}

  /**
   * Converts row/col index (base-0) to gid (base-1 global ID to stripes)
   * This convertion only supports one tile set image starts at 1.
   *
   * @param location2D
   * @param width max number of col.
   * @return
   */
  @VisibleForTesting static int getGid(Location2D location2D, int width) {
    return width * location2D.getRowIndex() + location2D.getColIndex() + 1;
  }

  static Location2D getLocation(int gid, int width) {
    return Location2D.of((gid - 1) / width, (gid - 1) % width);
  }
}
