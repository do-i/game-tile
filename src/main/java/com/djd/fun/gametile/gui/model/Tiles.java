package com.djd.fun.gametile.gui.model;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.inject.Inject;

import com.djd.fun.gametile.annotation.AccessibleTileIndex;
import com.djd.fun.gametile.model.Location2D;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.inject.assistedinject.Assisted;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.djd.fun.gametile.gui.model.TileUtil.getGid;

public class Tiles {

  private static final Logger log = LoggerFactory.getLogger(Tiles.class);

  // Number of sprites in a row
  private final int numOfTilesPerRow;
  private final ImmutableList<Tile> tiles;

  @Inject
  public Tiles(@Assisted URL tilesURL, @Assisted ImmutableSize tileDimension,
      @AccessibleTileIndex ImmutableSet<Integer> accessibleTileIndex) {
    try {
      BufferedImage bigImg = ImageIO.read(tilesURL);
      this.numOfTilesPerRow = bigImg.getWidth() / tileDimension.getWidth();
      tiles = createTiles(bigImg, tileDimension, accessibleTileIndex);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public Tile getTile(Location2D location) {
    try {
      return getTile(getGid(location, numOfTilesPerRow));
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("There is no tile for location=" + location);
    }
  }

  public Tile getTile(int gid) {
    if (gid <= 0 || gid > tiles.size()) {
      throw new IllegalArgumentException("There is no tile for gid=" + gid);
    }
    /* Note gid is base-1. tiles is base-0 */
    return tiles.get(gid - 1);
  }

  private static ImmutableList<Tile> createTiles(BufferedImage bigImg, ImmutableSize groundTileSize,
      ImmutableSet<Integer> accessibleTileIndex) {
    ImmutableList.Builder<Tile> tiles = ImmutableList.builder();
    final int width = bigImg.getWidth();
    final int height = bigImg.getHeight();
    final int tileWidth = groundTileSize.getWidth();
    final int tileHeight = groundTileSize.getHeight();
    log.info("bigImg: w={}, h={}", width, height);

    int tileIndex = 1;
    /*
      Iterate by row order. That is each row repeat all cols.
      y is row (top -> down), x is col (left -> right).
     */
    for (int y = 0; y < height; y += tileHeight) {
      for (int x = 0; x < width; x += tileWidth) {
        BufferedImage bufferedImage = bigImg.getSubimage(x, y, tileWidth, tileHeight);
        tiles.add(new Tile(String.format("(%d,%d)", x, y),
            accessibleTileIndex.contains(tileIndex++), bufferedImage));
      }
    }
    return tiles.build();
  }
}
