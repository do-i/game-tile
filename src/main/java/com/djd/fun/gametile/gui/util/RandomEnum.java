package com.djd.fun.gametile.gui.util;

import java.security.SecureRandom;

/**
 * TODO convert Guice instance
 *
 * Usage: RandomEnum.randomEnum(MyEnum.class);
 * https://stackoverflow.com/questions/1972392/pick-a-random-value-from-an-enum/14257525
 */
public class RandomEnum {

  private final static SecureRandom random = new SecureRandom();

  public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
    int x = random.nextInt(clazz.getEnumConstants().length);
    return clazz.getEnumConstants()[x];
  }
}
