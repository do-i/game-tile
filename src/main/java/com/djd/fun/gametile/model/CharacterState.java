package com.djd.fun.gametile.model;

import java.util.Optional;
import java.util.concurrent.locks.StampedLock;

import javax.inject.Inject;

import com.djd.fun.gametile.service.Location2DService;
import com.djd.fun.gametile.tiled.TiledDataResolver;
import com.djd.fun.gametile.tiled.model.TiledDataRequest;
import com.google.inject.assistedinject.Assisted;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.djd.fun.gametile.model.Direction.SOUTH;
import static com.djd.fun.gametile.model.Posture.CENTER;
import static com.djd.fun.gametile.model.Posture.LEFT;
import static com.djd.fun.gametile.model.Posture.RIGHT;

public class CharacterState {

  private static final Logger log = LoggerFactory.getLogger(CharacterState.class);
  private final StampedLock stampedLock = new StampedLock();
  private final Location2DService location2DService;
  private final TiledDataResolver tiledDataResolver;
  private final Location2D initialLocation;

  /* Below variables are stateful */

  private Location2D activeLocation;
  private Posture activePosture;
  private Direction activeDirection;
  private boolean isActive;

  @Inject CharacterState(TiledDataResolver tiledDataResolver, Location2DService location2DService,
      @Assisted Location2D initialLocation) {
    this.location2DService = location2DService;
    this.tiledDataResolver = tiledDataResolver;
    this.initialLocation = initialLocation;
    init();
  }

  public void init() {
    this.activeDirection = SOUTH;
    this.activePosture = CENTER;
    this.activeLocation = initialLocation;
    this.isActive = true;
  }

  public boolean isActive() {
    long stamp = stampedLock.readLock();
    try {
      return isActive;
    } finally {
      stampedLock.unlockRead(stamp);
    }
  }

  public void deactivate() {
    long stamp = stampedLock.writeLock();
    try {
      isActive = false;
    } finally {
      stampedLock.unlockWrite(stamp);
    }
  }

  public Posture getActivePosture() {
    long stamp = stampedLock.readLock();
    try {
      return activePosture;
    } finally {
      stampedLock.unlockRead(stamp);
    }
  }

  public Direction getActiveDirection() {
    long stamp = stampedLock.readLock();
    try {
      return activeDirection;
    } finally {
      stampedLock.unlockRead(stamp);
    }
  }

  public Location2D getActiveLocation() {
    long stamp = stampedLock.readLock();
    try {
      return activeLocation;
    } finally {
      stampedLock.unlockRead(stamp);
    }
  }

  public void moveTo(Location2D moveToLocation) {
    long stamp = stampedLock.writeLock();
    try {
      Optional<Direction> moveToDirection = location2DService.findDirection(activeLocation, moveToLocation);
      moveToDirection.ifPresent(this::changeDirection);
      activeLocation = moveToLocation;
    } finally {
      stampedLock.unlockWrite(stamp);
    }
  }

  /**
   * Mostly used by player. But, let's take out validation logic out of this class and use moveTo instead.
   */
  public void moveTowards(Direction direction) {
    long stamp = stampedLock.writeLock();
    try {
      changeDirection(direction);
      Optional<Location2D> destLocationOption = location2DService.adjacentOf(activeLocation, direction);
      if (destLocationOption.isPresent()) {
        Location2D destLocation = destLocationOption.get();
        if (tiledDataResolver.isAccessible(TiledDataRequest.builder().location(destLocation).build())) {
          activeLocation = destLocation;
        }
      }
    } finally {
      stampedLock.unlockWrite(stamp);
    }
  }

  private void changeDirection(Direction destDirection) {
    if (activeDirection == destDirection) {
      activePosture = activePosture == LEFT ? RIGHT : LEFT;
    } else {
      activePosture = CENTER;
      activeDirection = destDirection;
    }
  }
}
