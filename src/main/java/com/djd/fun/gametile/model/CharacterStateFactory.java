package com.djd.fun.gametile.model;

public interface CharacterStateFactory {
  CharacterState createCharacter(Location2D initialLocation);
}
