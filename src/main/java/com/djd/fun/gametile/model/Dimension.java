package com.djd.fun.gametile.model;

public class Dimension extends IntegerPair {
  private Dimension(int width, int height) {
    super(width, height);
  }

  public static Dimension of(int width, int height) {
    if (width > 0 && height > 0) {
      return new Dimension(width, height);
    }
    throw new IllegalArgumentException("Dimension must be at least 1x1.");
  }

  public int getWidth() {
    return value1;
  }

  public int getHeight() {
    return value2;
  }
}
