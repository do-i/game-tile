package com.djd.fun.gametile.model;

public enum Direction {
  NORTH, EAST, SOUTH, WEST
}
