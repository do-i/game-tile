package com.djd.fun.gametile.model;

/**
 * Used as map key to sprite index.
 */
public enum Posture {
  LEFT, CENTER, RIGHT
}
