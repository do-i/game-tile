package com.djd.fun.gametile.model;

import java.util.EnumMap;

/**
 * A representation of 2D key mapping from {@link Direction} and {@link Posture} to sprite index.
 */
public class SpriteIndex {

  private final EnumMap<Direction, EnumMap<Posture, Integer>> spriteIndexByDirectionPosture;


  private SpriteIndex(Builder builder) {
    this.spriteIndexByDirectionPosture = builder.spriteIndexByDirectionPosture;
  }

  public int getIndex(Direction direction, Posture posture) {
    return spriteIndexByDirectionPosture.get(direction).get(posture);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private EnumMap<Direction, EnumMap<Posture, Integer>> spriteIndexByDirectionPosture =
        new EnumMap<>(Direction.class);

    public Builder put(Direction direction, Posture posture, int index) {
      EnumMap<Posture, Integer> indexByPosture = spriteIndexByDirectionPosture.get(direction);
      if (indexByPosture == null) {
        indexByPosture = new EnumMap<>(Posture.class);
        spriteIndexByDirectionPosture.put(direction, indexByPosture);
      }
      indexByPosture.put(posture, index);
      return this;
    }

    public SpriteIndex build() {
      return new SpriteIndex(this);
    }
  }
}
