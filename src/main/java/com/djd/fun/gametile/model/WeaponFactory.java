package com.djd.fun.gametile.model;

public interface WeaponFactory {
  WeaponState createWeaponLocation(Location2D initialLocation, Direction direction);
}
