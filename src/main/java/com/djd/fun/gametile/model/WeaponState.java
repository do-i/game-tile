package com.djd.fun.gametile.model;

import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.locks.StampedLock;

import javax.inject.Inject;

import com.djd.fun.gametile.service.Location2DService;
import com.google.inject.assistedinject.Assisted;

/**
 * mutable weapon location.
 * location is mutated by timer
 */
public class WeaponState {

  private final StampedLock stampedLock = new StampedLock();
  private final Location2DService location2DService;
  private final Direction direction;
  private boolean active;
  private Location2D activeLocation;

  @Inject
  public WeaponState(Location2DService location2DService,
      @Assisted Location2D initialLocation, @Assisted Direction direction) {
    this.location2DService = location2DService;
    this.direction = direction;
    this.active = true;
    this.activeLocation = initialLocation;
  }

  public boolean isActive() {
    long stamp = stampedLock.tryOptimisticRead();
    boolean activeTmp = active;
    if (stampedLock.validate(stamp)) {
      return activeTmp;
    }
    stamp = stampedLock.readLock();
    try {
      return active;
    } finally {
      stampedLock.unlockRead(stamp);
    }
  }

  public void deactivate() {
    long stamp = stampedLock.writeLock();
    try {
      active = false;
    } finally {
      stampedLock.unlockWrite(stamp);
    }
  }

  public Location2D getActiveLocation() {
    long stamp = stampedLock.readLock();
    try {
      return activeLocation;
    } finally {
      stampedLock.unlockRead(stamp);
    }
  }

  public boolean move() {
    long stamp = stampedLock.writeLock();
    try {
      Optional<Location2D> moveToLocation = location2DService.adjacentOf(activeLocation, direction);
      if (moveToLocation.isPresent()) {
        activeLocation = moveToLocation.get();
        return true;
      }
      active = false; // if I call deactivate it will be deadlock perhaps try reentrant lock
      return false;
    } finally {
      stampedLock.unlockWrite(stamp);
    }
  }

  @Override public String toString() {
    return new StringJoiner(", ", WeaponState.class.getSimpleName() + "[", "]")
        .add("direction=" + direction)
        .add("active=" + active)
        .add("activeLocation=" + activeLocation)
        .toString();
  }
}
