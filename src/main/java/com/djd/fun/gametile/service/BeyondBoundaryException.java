package com.djd.fun.gametile.service;

import com.djd.fun.gametile.model.Dimension;
import com.djd.fun.gametile.model.Location2D;

public class BeyondBoundaryException extends RuntimeException {
  BeyondBoundaryException(Dimension dimension, Location2D location) {
    super(String.format("Beyong bound location: dimension=%s, location=%s", dimension, location));
  }
}
