package com.djd.fun.gametile.service;

import java.util.Optional;

import com.djd.fun.gametile.model.Direction;
import com.djd.fun.gametile.model.Location2D;

public interface Location2DService {

  /**
   * Finds location in the specified direction from the given location.
   *
   * @param location
   * @return {@link Location2D} location towards the given direction of the given location
   */
  Optional<Location2D> adjacentOf(Location2D location, Direction direction);

  /**
   * Finds location in the north direction from the given location.
   *
   * @param location
   * @return {@link Location2D} location north of given location
   */
  Optional<Location2D> northOf(Location2D location);

  /**
   * Finds location in the east direction from the given location.
   *
   * @param location
   * @return {@link Location2D} location east of given location
   */
  Optional<Location2D> eastOf(Location2D location);

  /**
   * Finds location in the south direction from the given location.
   *
   * @param location
   * @return {@link Location2D} location south of given location
   */
  Optional<Location2D> southOf(Location2D location);

  /**
   * Finds location in the west direction from the given location.
   *
   * @param location
   * @return {@link Location2D} location west of given location
   */
  Optional<Location2D> westOf(Location2D location);

  /**
   * Finds direction of move between two locations.
   * @return {@link Optional} of {@link Direction} or {@link Optional#empty()} if from and to are the same.
   */
  Optional<Direction> findDirection(Location2D from, Location2D to);
}
