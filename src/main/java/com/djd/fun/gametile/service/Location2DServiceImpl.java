package com.djd.fun.gametile.service;

import java.util.Optional;

import javax.inject.Inject;

import com.djd.fun.gametile.model.Dimension;
import com.djd.fun.gametile.model.Direction;
import com.djd.fun.gametile.model.Location2D;
import com.google.common.annotations.VisibleForTesting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.djd.fun.gametile.model.Direction.EAST;
import static com.djd.fun.gametile.model.Direction.NORTH;
import static com.djd.fun.gametile.model.Direction.SOUTH;
import static com.djd.fun.gametile.model.Direction.WEST;

/**
 * Location generator based on given location it generates relative location in a grid boundary
 * <p>
 * TODO use Decorator design pattern by making this class interface and Base class, collison with terrain property be subclass
 */
public class Location2DServiceImpl implements Location2DService {

  private static final Logger log = LoggerFactory.getLogger(Location2DServiceImpl.class);
  private final Dimension dimension;

  @Inject
  public Location2DServiceImpl(Dimension dimension) {
    this.dimension = dimension;
  }

  @Override
  public Optional<Location2D> adjacentOf(Location2D location, Direction direction) {
    switch (direction) {
      case NORTH:
        return northOf(location);
      case EAST:
        return eastOf(location);
      case SOUTH:
        return southOf(location);
      case WEST:
        return westOf(location);
    }
    throw new IllegalStateException("Undefined direction: " + direction);
  }

  @Override
  public Optional<Location2D> northOf(Location2D location) {
    return checkBoundary(Location2D.of(location.getRowIndex() - 1, location.getColIndex()));
  }

  @Override
  public Optional<Location2D> eastOf(Location2D location) {
    return checkBoundary(Location2D.of(location.getRowIndex(), location.getColIndex() + 1));
  }

  @Override
  public Optional<Location2D> southOf(Location2D location) {
    return checkBoundary(Location2D.of(location.getRowIndex() + 1, location.getColIndex()));
  }

  @Override
  public Optional<Location2D> westOf(Location2D location) {
    return checkBoundary(Location2D.of(location.getRowIndex(), location.getColIndex() - 1));
  }

  @Override
  public Optional<Direction> findDirection(Location2D from, Location2D to) {
    if (from.equals(to)) {
      return Optional.empty();
    }
    if (from.getColIndex() == to.getColIndex()) {
      return Optional.of(from.getRowIndex() > to.getRowIndex() ? NORTH : SOUTH);
    }
    if (from.getRowIndex() == to.getRowIndex()) {
      return Optional.of(from.getColIndex() > to.getColIndex() ? WEST : EAST);
    }
    throw new IllegalStateException(String.format("Unexpected from=%s, to=%s", from, to));
  }

  /**
   * Checks whether given location is within the boundary.
   *
   * @param location to be validated
   * @return {@link Location2D} if it is within the boundary
   * @throws BeyondBoundaryException
   */
  @VisibleForTesting Optional<Location2D> checkBoundary(Location2D location) {
    if (location.getRowIndex() >= 0
        && location.getRowIndex() < dimension.getHeight()
        && location.getColIndex() >= 0
        && location.getColIndex() < dimension.getWidth()) {
      return Optional.of(location);
    }
    log.debug("OutOfBoundary: dimension={}, location={}", dimension, location);
    return Optional.empty();
  }
}
