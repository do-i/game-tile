package com.djd.fun.gametile.service;

import java.util.Comparator;
import java.util.Optional;

import javax.inject.Inject;

import com.djd.fun.gametile.model.Location2D;
import com.djd.fun.gametile.tiled.TiledDataResolver;
import com.djd.fun.gametile.tiled.model.TiledDataRequest;
import com.google.common.collect.ImmutableSet;

import static com.google.common.base.Predicates.not;

/**
 * Algorithm to find a shortest path from origin to destination using Manhattan distance.
 */
public class ManhattanMovement implements MovementService {

  private final Location2DService location2DService;
  private final TiledDataResolver tiledDataResolver;

  @Inject ManhattanMovement(Location2DService location2DService, TiledDataResolver tiledDataResolver) {
    this.location2DService = location2DService;
    this.tiledDataResolver = tiledDataResolver;
  }

  /**
   * @param origin current start location
   * @param destination destination location
   * @param obstacles inaccessible locations due to ocupied by other npc or non-accessible terrain
   * @return closest location to the destination if non found then origin location (no move)
   */
  @Override
  public Location2D computeNextMove(Location2D origin, Location2D destination, ImmutableSet<Location2D> obstacles) {
    ImmutableSet<Optional<Location2D>> nesw = ImmutableSet.<Optional<Location2D>>builder()
        .add(location2DService.northOf(origin))
        .add(location2DService.eastOf(origin))
        .add(location2DService.southOf(origin))
        .add(location2DService.westOf(origin))
        .build();

    /*
    Filter out locations that has obstacles or inaccessible terrain
     */
    ImmutableSet<Location2D> filteredLocations = nesw.stream()
        .filter(Optional::isPresent)
        .map(Optional::get)
        .filter(location->tiledDataResolver.isAccessible(TiledDataRequest.builder().location(location).build()))
        .filter(not(obstacles::contains))
        .collect(ImmutableSet.toImmutableSet());
    /*
   Compute the shortest distance between orign and destination using Manhattan distance algorithm.
     */
    return filteredLocations.stream()
        .min(Comparator.comparing(destination::computeManhattanDistance))
        .orElse(origin);
  }
}
