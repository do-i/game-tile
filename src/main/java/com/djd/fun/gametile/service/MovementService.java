package com.djd.fun.gametile.service;

import com.djd.fun.gametile.model.Location2D;
import com.google.common.collect.ImmutableSet;

public interface MovementService {

  /**
   * Finds a shortest path from origin location to destination location.
   *
   * @param origin current start location
   * @param destination destination location
   * @param obstacles inaccessible locations due to ocupied by other npc or non-accessible terrain
   * @return {@link Location2D} next location that is towards the destination in shortest path
   */
  Location2D computeNextMove(Location2D origin, Location2D destination, ImmutableSet<Location2D> obstacles);

}
