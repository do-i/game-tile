package com.djd.fun.gametile.tiled;

import java.io.IOException;
import java.net.URL;

import com.djd.fun.gametile.tiled.model.TiledMapData;
import com.djd.fun.gametile.tiled.model.sprite.BufferedSprites;
import com.djd.fun.io.MoreResources;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.google.common.io.Resources;
import com.google.inject.PrivateModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class TiledDataModule extends PrivateModule {

  @Override protected void configure() {
    bind(TiledDataResolver.class).in(Singleton.class);
    expose(TiledDataResolver.class);
  }

  // TODO I feel like BufferedSprites should be created via factory.
  @Provides @Singleton
  BufferedSprites provideTiledSprites(TiledMapData tiledMapData) {
    return new BufferedSprites(tiledMapData.getTiledTileSets());
  }

  @Provides @Singleton
  TiledMapData provideTiledMapData(ObjectMapper objectMapper, URL tiledDataFileURL) {
    try {
      return objectMapper.readValue(tiledDataFileURL, TiledMapData.class);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Provides @Singleton
  ObjectMapper provideObjectMapper(XmlMapper impl) {
    impl.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    impl.registerModule(new GuavaModule());
    impl.enable(SerializationFeature.INDENT_OUTPUT);
    return impl;
  }

  @Provides @Singleton
  URL provideTiledDataFileURL() {
    return MoreResources.getResource("layers_level_001.tmx");
  }

}
