package com.djd.fun.gametile.tiled;

import java.awt.image.BufferedImage;
import java.util.Optional;

import javax.inject.Inject;

import com.djd.fun.gametile.model.Location2D;
import com.djd.fun.gametile.tiled.model.TiledData;
import com.djd.fun.gametile.tiled.model.TiledDataRequest;
import com.djd.fun.gametile.tiled.model.TiledGroup;
import com.djd.fun.gametile.tiled.model.TiledLayer;
import com.djd.fun.gametile.tiled.model.TiledMapData;
import com.djd.fun.gametile.tiled.model.sprite.BufferedSprites;
import com.google.common.collect.ImmutableList;

import static com.djd.fun.gametile.tiled.model.TiledDataRequest.LAYER_2_INACCESSIBLE_BG;
import static com.djd.fun.gametile.tiled.model.TiledDataRequest.LAYER_4_INACCESSIBLE_FG;
import static com.djd.fun.util.MorePreconditions.checkPositiveBoundedInt;

/**
 * The gateway to Tiled data files, layers_level_n.tmx as well as referenced png files.
 * With {@link TiledDataRequest} the class provides corresponding sprite as well as tile accessibility
 * information.
 */
public class TiledDataResolver {

  private final TiledMapData tiledMapData;
  private final BufferedSprites bufferedSprites;
  private final ImmutableList<TiledGroup> tiledGroups;

  @Inject TiledDataResolver(TiledMapData tiledMapData, BufferedSprites bufferedSprites) {
    this.tiledMapData = tiledMapData;
    this.bufferedSprites = bufferedSprites;
    this.tiledGroups = tiledMapData.getTiledGroups();
  }

  /**
   * @return number of columns
   */
  public int getMapWidth() {
    return tiledMapData.getWidth();
  }

  /**
   * @return number of rows
   */
  public int getMapHeight() {
    return tiledMapData.getHeight();
  }

  /**
   * Gets sprite for given {@link TiledDataRequest}. If the location has 0, it returns {@link Optional#empty()}
   *
   * @param tiledDataRequest
   * @return {@link Optional} of {@link BufferedImage} for given {@link TiledDataRequest}
   */
  public Optional<BufferedImage> getSprite(TiledDataRequest tiledDataRequest) {
    int gid = findGid(tiledDataRequest);
    return gid > 0 ? Optional.of(getSpriteAt(gid)) : Optional.empty();
  }

  /**
   * Retrieve sprite for a given global ID
   *
   * @param globalId starts 1 to available number of tiles
   * @return {@link BufferedImage} for a given global ID
   * @throws java.util.NoSuchElementException
   */
  public BufferedImage getSpriteAt(int globalId) {
    return bufferedSprites.getSprite(globalId);
  }

  /**
   * Determine two inaccessible layers. If both has {@code 0} then it is considered accessible.
   *
   * @param tiledDataRequest
   * @return {@code true} if a given level and location is accessible. {@code false} otherwise.
   */
  public boolean isAccessible(TiledDataRequest tiledDataRequest) {
    // make sure layerIds are set correctly
    return ImmutableList.of(
        TiledDataRequest.builder(tiledDataRequest).layerName(LAYER_2_INACCESSIBLE_BG).build(),
        TiledDataRequest.builder(tiledDataRequest).layerName(LAYER_4_INACCESSIBLE_FG).build())
        .stream()
        .map(this::findGid)
        .allMatch(gid -> gid == 0);
  }

  /**
   * Searches treasure location. It is linear search and relatively expensive. So, the result should
   * be cached by its client.
   *
   * @param tiledDataRequest level and layerId are matter
   * @return {@link Location2D}
   */
  public Location2D findTreasureLocation(TiledDataRequest tiledDataRequest) {
    TiledData tiledData = getTiledData(tiledDataRequest);
    for (int row = 0; row < tiledData.getRowCounts(); row++) {
      for (int col = 0; col < tiledData.getColCounts(); col++) {
        Location2D location = Location2D.of(row, col);
        if (tiledData.getGlobalIdAt(location) != 0) {
          return location;
        }
      }
    }
    throw new IllegalStateException(String.format("Could not find treasure in level {}", tiledDataRequest.getLevel()));
  }

  /**
   * @param tiledDataRequest level, layerId, and location matter.
   * @return gid if found, or 0 if not found
   */
  private int findGid(TiledDataRequest tiledDataRequest) {
    return getTiledData(tiledDataRequest).getGlobalIdAt(tiledDataRequest.getLocation());
  }

  /**
   * @param tiledDataRequest only level and layerId are matter
   * @return {@link TiledData} for given {@link TiledDataRequest}
   */
  public TiledData getTiledData(TiledDataRequest tiledDataRequest) {
    int level = checkPositiveBoundedInt(tiledDataRequest.getLevel(), tiledGroups.size());
    TiledLayer tiledLayer = tiledGroups.get(level - 1).getTiledLayer(tiledDataRequest.getLayerName());
    TiledData tiledData = tiledLayer.getTiledData();
    return tiledData;
  }
}
