package com.djd.fun.gametile.tiled.model;

import java.util.Arrays;
import java.util.List;

import com.djd.fun.gametile.model.Location2D;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import static com.google.common.base.Preconditions.checkState;

public class TiledData {

  private static final Splitter ON_COMMA = Splitter.on(',').trimResults().omitEmptyStrings();
  private final int rowCounts;
  private final int colCounts;
  private final int[][] globalIdData;

  /**
   * @param data
   * @param rowCount number of rows
   * @param colCount number of columns
   */
  public TiledData(String data, int rowCount, int colCount) {
    this.globalIdData = new int[rowCount][colCount];
    List<String> sprites = ON_COMMA.splitToList(data);
    checkState(sprites.size() == rowCount * colCount,
        String.format("available sprites=%d, expected sprites=%d", sprites.size(), rowCount * colCount));
    int rowIndex = 0;
    for (List<String> rows : Lists.partition(sprites, colCount)) {
      int colIndex = 0;
      for (String globalId : rows) {
        globalIdData[rowIndex][colIndex] = Integer.parseInt(globalId);
        ++colIndex;
      }
      ++rowIndex;
    }
    this.rowCounts = rowCount;
    this.colCounts = colCount;
  }

  public int getRowCounts() {
    return rowCounts;
  }

  public int getColCounts() {
    return colCounts;
  }

  public int getGlobalIdAt(Location2D location) {
    return globalIdData[location.getRowIndex()][location.getColIndex()];
  }

  public String getGlobalIdData() {
    return Arrays.deepToString(globalIdData);
  }

  @Override public String toString() {
    return getGlobalIdData();
  }
}
