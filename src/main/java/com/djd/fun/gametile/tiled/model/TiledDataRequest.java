package com.djd.fun.gametile.tiled.model;

import java.util.Objects;

import com.djd.fun.gametile.model.Location2D;
import com.google.common.collect.ImmutableList;

import static com.djd.fun.util.MorePreconditions.checkPositiveInt;
import static com.google.common.base.Preconditions.checkNotNull;

public class TiledDataRequest {

  public static final String LAYER_1_ACCESSIBLE_BG = "Layer1_accessible_bg";
  public static final String LAYER_2_INACCESSIBLE_BG = "Layer2_inaccessible_bg";
  public static final String LAYER_3_ACCESSIBLE_FG = "Layer3_accessible_fg";
  public static final String LAYER_4_INACCESSIBLE_FG = "Layer4_inaccessible_fg";
  public static final String LAYER_5_TREASURE_FG = "Layer5_treasure";
  public static final String LAYER_6_TOP = "Layer6_top";
  private static final ImmutableList<String> TERRAIN_LAYER_NAMES = ImmutableList.of(
      LAYER_1_ACCESSIBLE_BG, LAYER_2_INACCESSIBLE_BG, LAYER_3_ACCESSIBLE_FG, LAYER_4_INACCESSIBLE_FG);

  private final int level;
  private final String layerName;
  private final Location2D location;

  public TiledDataRequest(Builder builder) {
    this.level = checkPositiveInt(builder.level);
    this.layerName = checkNotNull(builder.layerName);
    this.location = checkNotNull(builder.location);
  }

  public int getLevel() {
    return level;
  }

  public String getLayerName() {
    return layerName;
  }

  public Location2D getLocation() {
    return location;
  }

  public static TiledDataRequest with(int level, String layer, Location2D location) {
    return builder().level(level).layerName(layer).location(location).build();
  }

  public static ImmutableList<String> getTerrainLayerNames() {
    return TERRAIN_LAYER_NAMES;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static Builder builder(TiledDataRequest tiledDataRequest) {
    return builder().fromPrototype(tiledDataRequest);
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TiledDataRequest that = (TiledDataRequest)o;
    return level == that.level &&
        Objects.equals(layerName, that.layerName) &&
        Objects.equals(location, that.location);
  }

  @Override public int hashCode() {
    return Objects.hash(level, layerName, location);
  }

  public static class Builder {
    private int level = 1;
    private String layerName = LAYER_1_ACCESSIBLE_BG;
    private Location2D location = Location2D.of(-1,-1);

    public Builder fromPrototype(TiledDataRequest tiledDataRequest) {
      this.level = tiledDataRequest.level;
      this.layerName = tiledDataRequest.layerName;
      this.location = tiledDataRequest.location;
      return this;
    }

    public Builder level(int level) {
      this.level = level;
      return this;
    }

    public Builder layerName(String layerName) {
      this.layerName = layerName;
      return this;
    }

    public Builder location(Location2D location) {
      this.location = location;
      return this;
    }

    public TiledDataRequest build() {
      return new TiledDataRequest(this);
    }
  }
}
