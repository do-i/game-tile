package com.djd.fun.gametile.tiled.model;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

@JacksonXmlRootElement(localName = "group")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TiledGroup {
  private final String name;

  @Deprecated(/* use tiledLayerByName */)
  private final ImmutableList<TiledLayer> tiledLayers;
  private final ImmutableMap<String, TiledLayer> tiledLayerByName;

  @JsonCreator
  public TiledGroup(Builder builder) {
    this.name = builder.name;
    this.tiledLayers = builder.layers.build();
    this.tiledLayerByName = Maps.uniqueIndex(builder.layers.build(), TiledLayer::getName);
  }

  public String getName() {
    return name;
  }

  @Deprecated(/* use getTiledLayer(name) */)
  public ImmutableList<TiledLayer> getTiledLayers() {
    return tiledLayers;
  }

  /**
   * Finds a {@link TiledLayer} for a given name.
   *
   * @param name name of the tiled layer
   * @return {@link TiledLayer} for a specified name.
   * @throws IllegalArgumentException if an invalid name is specified
   */
  public TiledLayer getTiledLayer(String name) {
    TiledLayer tiledLayer = tiledLayerByName.get(name);
    if (tiledLayer == null) {
      throw new IllegalArgumentException(String.format("No tile layer found for %s", name));
    }
    return tiledLayer;
  }

  @Override public String toString() {
    return new StringJoiner(", ", TiledGroup.class.getSimpleName() + "[", "]")
        .add("tiledLayers=" + tiledLayerByName)
        .toString();
  }

  public static class Builder {
    private String name;
    private ImmutableList.Builder<TiledLayer> layers = ImmutableList.builder();

    @JsonSetter("name")
    public Builder name(String name) {
      this.name = name;
      return this;
    }

    @JsonSetter("layer")
    public Builder layer(TiledLayer tiledLayer) {
      this.layers.add(tiledLayer);
      return this;
    }

    public TiledGroup build() {
      return new TiledGroup(this);
    }
  }
}
