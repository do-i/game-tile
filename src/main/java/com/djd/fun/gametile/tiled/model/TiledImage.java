package com.djd.fun.gametile.tiled.model;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "image")
public class TiledImage {
  private final String source;
  private final int width;
  private final int height;

  @JsonCreator
  public TiledImage(Builder builder) {
    this.source = builder.source;
    this.width = builder.width;
    this.height = builder.height;
  }

  public String getSource() {
    return source;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  @Override public String toString() {
    return new StringJoiner(", ", TiledImage.class.getSimpleName() + "[", "]")
        .add("source='" + source + "'")
        .add("width=" + width)
        .add("height=" + height)
        .toString();
  }

  public static class Builder {
    private String source;
    private int width;
    private int height;

    @JsonSetter("source")
    public Builder source(String source) {
      this.source = source;
      return this;
    }

    @JsonSetter("width")
    public Builder width(int width) {
      this.width = width;
      return this;
    }

    @JsonSetter("height")
    public Builder height(int height) {
      this.height = height;
      return this;
    }

    public TiledImage build() {
      return new TiledImage(this);
    }
  }
}
