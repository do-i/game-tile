package com.djd.fun.gametile.tiled.model;


import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "layer")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TiledLayer {
  private final String name;
  private final int width;
  private final int height;
  private final TiledData tiledData;

  @JsonCreator
  public TiledLayer(Builder builder) {
    this.name = builder.name;
    this.width = builder.width;
    this.height = builder.height;
    this.tiledData = new TiledData(builder.data, height, width);
  }

  public String getName() {
    return name;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public TiledData getTiledData() {
    return tiledData;
  }

  @Override public String toString() {
    return new StringJoiner(", ", TiledLayer.class.getSimpleName() + "[", "]")
        .add("name='" + name + "'")
        .add("width=" + width)
        .add("height=" + height)
        .add("tiledData=" + tiledData)
        .toString();
  }

  public static class Builder {
    private String name;
    private int width;
    private int height;
    private String data;

    @JsonSetter("name")
    public Builder name(String name) {
      this.name = name;
      return this;
    }

    @JsonSetter("width")
    public Builder width(int width) {
      this.width = width;
      return this;
    }

    @JsonSetter("height")
    public Builder height(int height) {
      this.height = height;
      return this;
    }

    @JsonSetter("data")
    public Builder data(String data) {
      this.data = data;
      return this;
    }

    public TiledLayer build() {
      return new TiledLayer(this);
    }
  }
}