package com.djd.fun.gametile.tiled.model;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.google.common.collect.ImmutableList;

@JacksonXmlRootElement(localName = "map")
public class TiledMapData {
  private final String version;
  private final String tiledVersion;
  private final String orientation;
  private final String renderOrder;
  private final int width;
  private final int height;
  private final int tileWidth;
  private final int tileHeight;
  private final int infinite;
  private final int nextObjectid;
  private final ImmutableList<TiledTileSet> tiledTileSets;
  private final ImmutableList<TiledGroup> tiledGroups;

  @JsonCreator
  public TiledMapData(Builder builder) {
    this.version = builder.version;
    this.tiledVersion = builder.tiledVersion;
    this.orientation = builder.orientation;
    this.renderOrder = builder.renderOrder;
    this.width = builder.width;
    this.height = builder.height;
    this.tileWidth = builder.tileWidth;
    this.tileHeight = builder.tileHeight;
    this.infinite = builder.infinite;
    this.nextObjectid = builder.nextObjectId;
    this.tiledTileSets = builder.tileSets.build();
    this.tiledGroups = builder.groups.build();
  }

  public String getVersion() {
    return version;
  }

  public String getTiledVersion() {
    return tiledVersion;
  }

  public String getOrientation() {
    return orientation;
  }

  public String getRenderOrder() {
    return renderOrder;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public int getTileWidth() {
    return tileWidth;
  }

  public int getTileHeight() {
    return tileHeight;
  }

  public int getInfinite() {
    return infinite;
  }

  public int getNextObjectid() {
    return nextObjectid;
  }

  public ImmutableList<TiledTileSet> getTiledTileSets() {
    return tiledTileSets;
  }

  public ImmutableList<TiledGroup> getTiledGroups() {
    return tiledGroups;
  }

  public static class Builder {
    private String version;
    private String tiledVersion;
    private String orientation;
    private String renderOrder;
    private int width;
    private int height;
    private int tileWidth;
    private int tileHeight;
    private int infinite;
    private int nextObjectId;
    private ImmutableList.Builder<TiledTileSet> tileSets = ImmutableList.builder();
    private ImmutableList.Builder<TiledGroup> groups = ImmutableList.builder();

    @JsonSetter("version")
    public Builder setVersion(String version) {
      this.version = version;
      return this;
    }

    @JsonSetter("tiledversion")
    public Builder setTiledVersion(String tiledVersion) {
      this.tiledVersion = tiledVersion;
      return this;
    }

    @JsonSetter("orientation")
    public Builder setOrientation(String orientation) {
      this.orientation = orientation;
      return this;
    }

    @JsonSetter("renderorder")
    public Builder setRenderOrder(String renderOrder) {
      this.renderOrder = renderOrder;
      return this;
    }

    @JsonSetter("width")
    public Builder setWidth(int width) {
      this.width = width;
      return this;
    }

    @JsonSetter("height")
    public Builder setHeight(int height) {
      this.height = height;
      return this;
    }

    @JsonSetter("tilewidth")
    public Builder setTileWidth(int tileWidth) {
      this.tileWidth = tileWidth;
      return this;
    }

    @JsonSetter("tileheight")
    public Builder setTileHeight(int tileHeight) {
      this.tileHeight = tileHeight;
      return this;
    }

    @JsonSetter("infinite")
    public Builder setInfinite(int infinite) {
      this.infinite = infinite;
      return this;
    }

    @JsonSetter("nextobjectid")
    public Builder setNextObjectId(int nextObjectId) {
      this.nextObjectId = nextObjectId;
      return this;
    }

    @JsonSetter("tileset")
    public Builder setGroups(TiledTileSet tiledTileSet) {
      this.tileSets.add(tiledTileSet);
      return this;
    }

    @JsonSetter("group")
    public Builder setGroups(TiledGroup tiledGroup) {
      this.groups.add(tiledGroup);
      return this;
    }

    public TiledMapData build() {
      return new TiledMapData(this);
    }
  }

  @Override public String toString() {
    return new StringJoiner(", ", TiledMapData.class.getSimpleName() + "[", "]")
        .add("version='" + version + "'")
        .add("tiledVersion='" + tiledVersion + "'")
        .add("orientation='" + orientation + "'")
        .add("renderOrder='" + renderOrder + "'")
        .add("width=" + width)
        .add("height=" + height)
        .add("tileWidth=" + tileWidth)
        .add("tileHeight=" + tileHeight)
        .add("infinite=" + infinite)
        .add("nextObjectid=" + nextObjectid)
        .add("tiledTileSets=" + tiledTileSets)
        .add("tiledGroups=" + tiledGroups)
        .toString();
  }
}
