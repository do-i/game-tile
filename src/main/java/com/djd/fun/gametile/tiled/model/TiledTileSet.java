package com.djd.fun.gametile.tiled.model;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "tileset")
public class TiledTileSet {
  private final int firstGid;
  private final String name;
  private final int tileWidth;
  private final int tileHeight;
  private final int tileCount;
  private final int columns;
  private final TiledImage tiledImage;

  @JsonCreator
  public TiledTileSet(Builder builder) {
    this.firstGid = builder.firstGid;
    this.name = builder.name;
    this.tileWidth = builder.tileWidth;
    this.tileHeight = builder.tileHeight;
    this.tileCount = builder.tileCount;
    this.columns = builder.columns;
    this.tiledImage = builder.tiledImage;
  }

  public int getFirstGid() {
    return firstGid;
  }

  public String getName() {
    return name;
  }

  public int getTileWidth() {
    return tileWidth;
  }

  public int getTileHeight() {
    return tileHeight;
  }

  public int getTileCount() {
    return tileCount;
  }

  public int getColumns() {
    return columns;
  }

  @JsonGetter("image")
  public TiledImage getTiledImage() {
    return tiledImage;
  }

  @Override public String toString() {
    return new StringJoiner(", ", TiledTileSet.class.getSimpleName() + "[", "]")
        .add("firstGid=" + firstGid)
        .add("name='" + name + "'")
        .add("tileWidth=" + tileWidth)
        .add("tileHeight=" + tileHeight)
        .add("tileCount=" + tileCount)
        .add("columns=" + columns)
        .add("tiledImage=" + tiledImage)
        .toString();
  }

  public static class Builder {
    private int firstGid;
    private String name;
    private int tileWidth;
    private int tileHeight;
    private int tileCount;
    private int columns;
    private TiledImage tiledImage;

    @JsonSetter("firstgid")
    public Builder firstGid(int firstGid) {
      this.firstGid = firstGid;
      return this;
    }

    @JsonSetter("name")
    public Builder name(String name) {
      this.name = name;
      return this;
    }

    @JsonSetter("tilewidth")
    public Builder tileWidth(int tileWidth) {
      this.tileWidth = tileWidth;
      return this;
    }

    @JsonSetter("tileheight")
    public Builder tileHeight(int tileHeight) {
      this.tileHeight = tileHeight;
      return this;
    }

    @JsonSetter("tilecount")
    public Builder tileCount(int tileCount) {
      this.tileCount = tileCount;
      return this;
    }

    @JsonSetter("columns")
    public Builder columns(int columns) {
      this.columns = columns;
      return this;
    }

    @JsonSetter("image")
    public Builder image(TiledImage tiledImage) {
      this.tiledImage = tiledImage;
      return this;
    }

    public TiledTileSet build() {
      return new TiledTileSet(this);
    }
  }

}
