package com.djd.fun.gametile.tiled.model.sprite;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.NoSuchElementException;

import javax.imageio.ImageIO;

import com.djd.fun.gametile.tiled.model.TiledImage;
import com.djd.fun.gametile.tiled.model.TiledTileSet;
import com.djd.fun.io.MoreResources;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Eager loaded cache for sprites. During an instance creation it eagerly loads all images and splits
 * them into specified tile size. These tile sized sprites are accessible via Global ID (a.k.a., gid).
 */
public class BufferedSprites {

  private static final Logger log = LoggerFactory.getLogger(BufferedSprites.class);
  private final ImmutableList<BufferedImage> sprites;

  /**
   * Sort tiledTileSets by firstGid field. So that final list of tiled sprites are ordered correctly.
   *
   * @param tiledTileSets
   */
  public BufferedSprites(ImmutableList<TiledTileSet> tiledTileSets) {
    this.sprites = tiledTileSets.stream()
        .sorted(Comparator.comparingInt(TiledTileSet::getFirstGid))
        .map(BufferedSprites::split)
        .flatMap(ImmutableList::stream)
        .collect(ImmutableList.toImmutableList());
    log.info("{} tiles loaded.", sprites.size());
  }

  /**
   * @param gid Global ID starts at 1 to N where N is number of sprites
   * @return {@link BufferedImage}
   * @throws java.util.NoSuchElementException
   */
  public BufferedImage getSprite(int gid) {
    if (gid <= 0 || gid > sprites.size()) {
      throw new NoSuchElementException("There is no tile for gid=" + gid);
    }
    /* Note gid is base-1. sprites is base-0 */
    return sprites.get(gid - 1);
  }

  private static ImmutableList<BufferedImage> split(TiledTileSet tiledTileSet) {
    log.info("loading tile set: {}", tiledTileSet.getName());

    TiledImage tiledImage = tiledTileSet.getTiledImage();
    URL bigImageURL = MoreResources.getResource(tiledImage.getSource());
    BufferedImage bufferedImage = silentRead(bigImageURL);
    final int width = tiledImage.getWidth();
    final int height = tiledImage.getHeight();
    final int tileWidth = tiledTileSet.getTileWidth();
    final int tileHeight = tiledTileSet.getTileHeight();
    Preconditions.checkState(bufferedImage.getWidth() == width, "Incompatible image width");
    Preconditions.checkState(bufferedImage.getHeight() == height, "Incompatible image height");

    ImmutableList.Builder<BufferedImage> tileSprites = ImmutableList.builder();
    /*
      Iterate by row order. That is each row repeat all cols.
      y is row (top -> down), x is col (left -> right).
     */
    for (int y = 0; y < height; y += tileHeight) {
      for (int x = 0; x < width; x += tileWidth) {
        tileSprites.add(bufferedImage.getSubimage(x, y, tileWidth, tileHeight));
      }
    }
    return tileSprites.build();
  }

  private static BufferedImage silentRead(URL imageURL) {
    try {
      return ImageIO.read(imageURL);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
