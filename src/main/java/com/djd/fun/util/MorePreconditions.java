package com.djd.fun.util;

public class MorePreconditions {

  /**
   * Ensures that {@code number} greater than equals to {@code 1}
   *
   * @param number a user-supplied number
   * @return the value of {@code number}
   * @throws IndexOutOfBoundsException if {@code number} is less than {@code 1}
   */
  public static int checkPositiveInt(int number) {
    if (number < 1) {
      throw new IllegalArgumentException(String.format("number=%d is not positive int", number));
    }
    return number;
  }

  /**
   * Ensures that {@code number} in a range {@code 1} and {@code max}
   *
   * @param number a user-supplied number
   * @param max largest possible number
   * @return the value of {@code number}
   * @throws IndexOutOfBoundsException if {@code number} is out of the specified range
   */
  public static int checkPositiveBoundedInt(int number, int max) {
    return checkNumberInRange(number, 1, max);
  }

  /**
   * Ensures that {@code number} in a range {@code from} and {@code to}
   *
   * @param number a user-supplied number
   * @param from smallest possible number
   * @param to largest possible number
   * @return the value of {@code number}
   * @throws IndexOutOfBoundsException if {@code number} is out of the specified range
   */
  public static int checkNumberInRange(int number, int from, int to) {
    if (number < from || number > to) {
      throw new IndexOutOfBoundsException(String.format("Not true : (%d <= %d <= %d)", from, number, to));
    }
    return number;
  }
}
