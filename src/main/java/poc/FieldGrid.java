package poc;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.google.common.base.Splitter;
import com.google.common.io.Resources;

public class FieldGrid {
  public static void main(String[] args) throws URISyntaxException, IOException {

    List<String> rows =
        Files.readAllLines(Paths.get(Resources.getResource("terrain.csv").toURI()));
    Splitter splitter = Splitter.on(',').trimResults().omitEmptyStrings();
    for (String row : rows) {
      for (String cel : splitter.split(row)) {
        System.out.print(" " + Integer.parseInt(cel));
      }
      System.out.println();
    }
  }
}
