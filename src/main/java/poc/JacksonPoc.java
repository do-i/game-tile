package poc;

import java.io.IOException;
import java.net.URL;

import com.djd.fun.gametile.tiled.model.TiledMapData;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.google.common.io.Resources;

public class JacksonPoc {

  public static final URL RESULT_FILE = Resources.getResource("layers_level_001.tmx");
  private static final ObjectMapper objectMapper = new XmlMapper();

  public static void main(String[] args) throws IOException {
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    objectMapper.registerModule(new GuavaModule());
    objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
//    xmlMapper.writeValue(RESULT_FILE, new SimpleBean(18, "Orange", new SimpleBean.Grain("soba")));
    TiledMapData fromFile = objectMapper.readValue(RESULT_FILE, TiledMapData.class);
    System.out.println(objectMapper.writeValueAsString(fromFile));
  }
}
