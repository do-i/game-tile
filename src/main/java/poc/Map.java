package poc;

import java.util.List;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.google.common.collect.ImmutableList;

public class Map {
  private final String version;
  private final String tiledVersion;
  private final String orientation;
  private final String renderOrder;
  private final int width;
  private final int height;
  private final int tileWidth;
  private final int tileHeight;
  private final int infinite;
  private final int nextObjectid;
  private final ImmutableList<Layer> layers;

  @JsonCreator
  public Map(Builder builder) {
    this.version = builder.version;
    this.tiledVersion = builder.tiledVersion;
    this.orientation = builder.orientation;
    this.renderOrder = builder.renderOrder;
    this.width = builder.width;
    this.height = builder.height;
    this.tileWidth = builder.tileWidth;
    this.tileHeight = builder.tileHeight;
    this.infinite = builder.infinite;
    this.nextObjectid = builder.nextObjectId;
    this.layers = builder.layers.build();
  }

  public List<Layer> getLayers() {
    return layers;
  }

  public static class Builder {

    private String version;
    private String tiledVersion;
    private String orientation;
    private String renderOrder;
    private int width;
    private int height;
    private int tileWidth;
    private int tileHeight;
    private int infinite;
    private int nextObjectId;
    private ImmutableList.Builder<Layer> layers = ImmutableList.builder();

    @JsonSetter("version")
    public Builder setVersion(String version) {
      this.version = version;
      return this;
    }

    @JsonSetter("tiledVersion")
    public Builder setTiledVersion(String tiledVersion) {
      this.tiledVersion = tiledVersion;
      return this;
    }

    @JsonSetter("orientation")
    public Builder setOrientation(String orientation) {
      this.orientation = orientation;
      return this;
    }

    @JsonSetter("renderorder")
    public Builder setRenderOrder(String renderOrder) {
      this.renderOrder = renderOrder;
      return this;
    }

    @JsonSetter("width")
    public Builder setWidth(int width) {
      this.width = width;
      return this;
    }

    @JsonSetter("height")
    public Builder setHeight(int height) {
      this.height = height;
      return this;
    }

    @JsonSetter("tilewidth")
    public Builder setTileWidth(int tileWidth) {
      this.tileWidth = tileWidth;
      return this;
    }

    @JsonSetter("tileheight")
    public Builder setTileHeight(int tileHeight) {
      this.tileHeight = tileHeight;
      return this;
    }

    @JsonSetter("infinite")
    public Builder setInfinite(int infinite) {
      this.infinite = infinite;
      return this;
    }

    @JsonSetter("nextobjectid")
    public Builder setNextObjectId(int nextObjectId) {
      this.nextObjectId = nextObjectId;
      return this;
    }

    @JsonSetter("layers")
    public Builder setLayers(Layer layer) {
      this.layers.add(layer);
      return this;
    }

    public Map build() {
      return new Map(this);
    }
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Layer {
    private final String name;
    private final int width;
    private final int height;
    private final String data;

    @JsonCreator
    public Layer(Builder builder) {
      this.name = builder.name;
      this.width = builder.width;
      this.height = builder.height;
      this.data = builder.data;
    }

    public String getName() {
      return name;
    }

    public int getWidth() {
      return width;
    }

    public int getHeight() {
      return height;
    }

    public String getData() {
      return data;
    }

    @Override public String toString() {
      return new StringJoiner(", ", Layer.class.getSimpleName() + "[", "]")
          .add("name='" + name + "'")
          .add("width=" + width)
          .add("height=" + height)
          .add("data=" + data)
          .toString();
    }

    public static class Builder {
      private String name;
      private int width;
      private int height;
      private String data;

      public Builder setName(String name) {
        this.name = name;
        return this;
      }

      public Builder setWidth(int width) {
        this.width = width;
        return this;
      }

      public Builder setHeight(int height) {
        this.height = height;
        return this;
      }

      public Builder setData(String data) {
        this.data = data;
        return this;
      }

      public Layer build() {
        return new Layer(this);
      }
    }
  }
}
