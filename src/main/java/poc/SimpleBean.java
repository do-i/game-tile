package poc;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSetter;

public class SimpleBean {
  private int apple;
  private String bean;
  private Grain grain;

  @JsonCreator
  public SimpleBean(@JsonSetter("apple") int apple, @JsonSetter("bean") String bean, @JsonSetter("grain") Grain grain) {
    this.apple = apple;
    this.bean = bean;
    this.grain = grain;
  }

  public int getApple() {
    return apple;
  }

  public String getBean() {
    return bean;
  }

  public Grain getGrain() {
    return grain;
  }

  @Override public String toString() {
    return new StringJoiner(", ", SimpleBean.class.getSimpleName() + "[", "]")
        .add("apple=" + apple)
        .add("bean='" + bean + "'")
        .add("grain=" + grain)
        .toString();
  }

  public static class Grain {
    private String cell;

    @JsonCreator
    public Grain(@JsonSetter("cell") String cell) {
      this.cell = cell;
    }

    public String getCell() {
      return cell;
    }

    @Override public String toString() {
      return new StringJoiner(", ", Grain.class.getSimpleName() + "[", "]")
          .add("cell='" + cell + "'")
          .toString();
    }
  }
}
