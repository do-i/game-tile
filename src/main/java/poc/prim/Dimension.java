package poc.prim;

import java.util.Objects;
import java.util.StringJoiner;

public class Dimension {
  private final int rowSize;
  private final int colSize;

  public Dimension(int rowSize, int colSize) {
    this.rowSize = rowSize;
    this.colSize = colSize;
  }

  public int getRowSize() {
    return rowSize;
  }

  public int getColSize() {
    return colSize;
  }

  public static Dimension of(int row, int col) {
    return new Dimension(row, col);
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Dimension dimension = (Dimension)o;
    return rowSize == dimension.rowSize &&
        colSize == dimension.colSize;
  }

  @Override public int hashCode() {
    return Objects.hash(rowSize, colSize);
  }

  @Override public String toString() {
    return new StringJoiner(", ", Dimension.class.getSimpleName() + "[", "]")
        .add("row=" + rowSize)
        .add("col=" + colSize)
        .toString();
  }
}
