package poc.prim;

import java.util.Objects;
import java.util.StringJoiner;

import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;

public class LocationChain {
  private final int rowIdx;
  private final int colIdx;
  private final @Nullable LocationChain parent;

  private LocationChain(int rowIdx, int colIdx, @Nullable LocationChain parent) {
    this.rowIdx = rowIdx;
    this.colIdx = colIdx;
    this.parent = parent;
  }

  public int getRowIdx() {
    return rowIdx;
  }

  public int getColIdx() {
    return colIdx;
  }

  public LocationChain createNorth() {
    return LocationChain.of(rowIdx - 1, colIdx, this);
  }

  public LocationChain createEast() {
    return LocationChain.of(rowIdx, colIdx + 1, this);
  }

  public LocationChain createSouth() {
    return LocationChain.of(rowIdx + 1, colIdx, this);
  }

  public LocationChain createWest() {
    return LocationChain.of(rowIdx, colIdx - 1, this);
  }

  /**
   * P: parent location
   * C: child location
   * N: new location
   * <p>
   * P and C are on the same row, and
   * P is lower column than C, then
   * N is on the same row and higher column than C
   * <pre>
   * # # #
   * P C N
   * # # #
   * </pre>
   * <p>
   * <p>
   * P and C are on the same row, and
   * P is higher column than C, then
   * N is on the same row and lower column than C
   * <pre>
   * # # #
   * N C P
   * # # #
   * </pre>
   * <p>
   * <p>
   * P and C are on the same column, and
   * P is lower row than C, then
   * N is on the same column and higher row than C
   * <pre>
   * # P #
   * # C #
   * # N #
   * </pre>
   * <p>
   * <p>
   * P and C are on the same column, and
   * P is higher row than C, then
   * N is on the same column and lower row than C
   * <pre>
   * # N #
   * # C #
   * # P #
   * </pre>
   */
  public LocationChain createOppositeFromParent() {
    checkNotNull(parent);
    if (this.equals(parent)) {
      throw new IllegalStateException("Cyclic reference.");
    }
    int newRow = rowIdx;
    int newCol = colIdx;
    if (rowIdx != parent.rowIdx) {
      if (rowIdx < parent.rowIdx) {
        newRow--;
      } else {
        newRow++;
      }
    }
    if (colIdx != parent.colIdx) {
      if (colIdx < parent.colIdx) {
        newCol--;
      } else {
        newCol++;
      }
    }
    return LocationChain.of(newRow, newCol, this);
  }

  public static LocationChain of(int rowIdx, int colIdx) {
    return new LocationChain(rowIdx, colIdx, null);
  }

  public static LocationChain of(int rowIdx, int colIdx, LocationChain parent) {
    return new LocationChain(rowIdx, colIdx, checkNotNull(parent));
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    LocationChain that = (LocationChain)o;
    return rowIdx == that.rowIdx &&
        colIdx == that.colIdx;
  }

  @Override public int hashCode() {
    return Objects.hash(rowIdx, colIdx);
  }

  @Override public String toString() {
    return new StringJoiner(", ", LocationChain.class.getSimpleName() + "[", "]")
        .add("rowIdx=" + rowIdx)
        .add("colIdx=" + colIdx)
        .add("parent=" + parent)
        .toString();
  }
}
