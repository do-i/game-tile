package poc.prim;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  private static final Logger log = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {
    log.info("Starting app");
    Random random = new Random();
    Dimension dimension = new Dimension(20, 50);
    MutableMatrix matrix = new MutableMatrix(dimension);
    Prim prim = new Prim(matrix, 10, random);
    prim.generate();
    matrix.printMatrix();
  }
}
