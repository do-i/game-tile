package poc.prim;

import java.util.function.Predicate;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkArgument;
import static poc.prim.MutableMatrix.FloorState.WALL;

public class MutableMatrix {

  private final Dimension dimension;

  enum FloorState {
    START('S'), END('E'), WALL('W'), FLOOR(' ');
    private char letter;

    FloorState(char letter) {
      this.letter = letter;
    }

    public char getLetter() {
      return letter;
    }
  }

  private final Predicate<LocationChain> boundary;
  private final FloorState[][] floorStateMatrix;

  public MutableMatrix(Dimension dimension) {
    int rowSize = dimension.getRowSize();
    int colSize = dimension.getColSize();
    this.dimension = dimension;
    checkArgument(rowSize > 1, "minimum rowSize is 2", rowSize);
    checkArgument(colSize > 1, "minimum colSize is 2", colSize);
    floorStateMatrix = new FloorState[rowSize][colSize];
    boundary = (location) -> {
      if (location.getRowIdx() < 0 || location.getRowIdx() >= dimension.getRowSize()) {
        return false;
      }
      if (location.getColIdx() < 0 || location.getColIdx() >= dimension.getColSize()) {
        return false;
      }
      return true;
    };
    // initialize the matrix
    for (int row = 0; row < rowSize; row++) {
      for (int col = 0; col < colSize; col++) {
        floorStateMatrix[row][col] = WALL;
      }
    }
  }

  public void changeStateAtLocation(FloorState state, LocationChain location) {
    checkArgument(boundary.test(location));
    floorStateMatrix[location.getRowIdx()][location.getColIdx()] = state;
  }

  public boolean isWallAt(LocationChain location) {
    return boundary.test(location)
        && floorStateMatrix[location.getRowIdx()][location.getColIdx()] == WALL;
  }

  /**
   * @param currentLocation
   * @return maximum adjacent non-wall locations.
   */
  public ImmutableList<LocationChain> findAdjacentWalls(LocationChain currentLocation) {
    return Stream.of(currentLocation.createNorth(),
        currentLocation.createEast(),
        currentLocation.createSouth(),
        currentLocation.createWest())
        .filter(boundary)
        .filter(location -> floorStateMatrix[location.getRowIdx()][location.getColIdx()] == WALL)
        .collect(ImmutableList.toImmutableList());
  }

  public Dimension getDimension() {
    return dimension;
  }

  public void printMatrix() {
    System.out.println("----");
    for (FloorState[] row : floorStateMatrix) {
      for (FloorState state : row) {
        System.out.print(state.getLetter() + " ");
      }
      System.out.println();
    }
  }
}
