package poc.prim;

import java.util.ArrayList;
import java.util.Random;

import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkArgument;
import static poc.prim.MutableMatrix.FloorState.END;
import static poc.prim.MutableMatrix.FloorState.FLOOR;
import static poc.prim.MutableMatrix.FloorState.START;

public class Prim {

  public static final int FOUR_CORNERS = 4;
  private final Dimension dimension;
  private final MutableMatrix matrix;
  private final int distance;
  private final Random random;


  public Prim(MutableMatrix matrix, int distance, Random random) {
    checkArgument(distance > 0, "Distance must be positive int");
    this.dimension = matrix.getDimension();
    this.matrix = matrix;
    this.distance = distance;
    this.random = random;
  }

  public void generate() {
    LocationChain startLocation = createStartLocation(distance);
    matrix.changeStateAtLocation(START, startLocation);
    ArrayList<LocationChain> mutableFrontierList =
        new ArrayList<>(dimension.getRowSize() * dimension.getColSize());
    // Find adjacent locations that is WALL to the startNode
    ImmutableList<LocationChain> frontiers = matrix.findAdjacentWalls(startLocation);
    // every element in frontiers list must have parent location (startNode is NOT a frontier)
    mutableFrontierList.addAll(frontiers);
    LocationChain lastLocation = startLocation;
    while (!mutableFrontierList.isEmpty()) {
      final LocationChain frontier = pickLocationAtRandom(mutableFrontierList);
      final LocationChain oppositeToParentLocation = frontier.createOppositeFromParent();
      if (matrix.isWallAt(oppositeToParentLocation)) {
        matrix.changeStateAtLocation(FLOOR, frontier);
        matrix.changeStateAtLocation(FLOOR, oppositeToParentLocation);
        mutableFrontierList.addAll(matrix.findAdjacentWalls(oppositeToParentLocation));
        lastLocation = oppositeToParentLocation;
      }
    }
    matrix.changeStateAtLocation(END, lastLocation);
  }

  private LocationChain createStartLocation() {
    int rowIdx = random.nextInt(dimension.getRowSize());
    int colIdx = random.nextInt(dimension.getColSize());
    return LocationChain.of(rowIdx, colIdx);
  }

  private LocationChain createStartLocation(int maxDistanceFromCorner) {
    checkArgument(maxDistanceFromCorner <
        Math.min(dimension.getRowSize(), dimension.getColSize()));
    int distance = random.nextInt(maxDistanceFromCorner);
    int newRowIdx;
    int newColIdx;
    switch (random.nextInt(FOUR_CORNERS)) {
      case 0:
        newRowIdx = distance;
        newColIdx = distance;
        break;
      case 1:
        newRowIdx = distance;
        newColIdx = dimension.getColSize() - 1 - distance;
        break;
      case 2:
        newRowIdx = dimension.getRowSize() - 1 - distance;
        newColIdx = dimension.getColSize() - 1 - distance;
        break;
      case 3:
        newRowIdx = dimension.getRowSize() - 1 - distance;
        newColIdx = distance;
        break;
      default:
        throw new IllegalStateException("Never reach here");
    }
    return LocationChain.of(newRowIdx, newColIdx);
  }

  /**
   * Randomly select {@link LocationChain} from the list.
   *
   * @param mutableFrontierList
   * @return {@link LocationChain} that is selected randomly
   * @implNote To avoid shift, remove last element and set at the randomly chosen location. If randomIndex is
   * last index, then skip set.
   */
  private LocationChain pickLocationAtRandom(ArrayList<LocationChain> mutableFrontierList) {
    // randomly pick a frontier from the list
    int size = mutableFrontierList.size();
    int lastIndex = size - 1;
    int randomIndex = random.nextInt(size);
    if (randomIndex == lastIndex) {
      return mutableFrontierList.remove(lastIndex);
    }
    LocationChain frontier = mutableFrontierList.get(randomIndex);
    mutableFrontierList.set(randomIndex, mutableFrontierList.remove(lastIndex));
    return frontier;
  }
}
