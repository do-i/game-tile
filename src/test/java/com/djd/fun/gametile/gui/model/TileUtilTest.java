package com.djd.fun.gametile.gui.model;

import com.djd.fun.gametile.model.Location2D;

import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

class TileUtilTest {

  private static final int GRID_SIZE = 5; // 5x5 grid

  @Test
  void getGid() {
    int gid = 1;
    for (int row = 0; row < GRID_SIZE; row++) {
      for (int col = 0; col < GRID_SIZE; col++) {
        assertThat(TileUtil.getGid(Location2D.of(row, col), GRID_SIZE)).isEqualTo(gid++);
      }
    }
  }

  @Test
  void getLocation() {
    int gid = 1;
    for (int row = 0; row < GRID_SIZE; row++) {
      for (int col = 0; col < GRID_SIZE; col++) {
        assertThat(TileUtil.getLocation(gid++, GRID_SIZE)).isEqualTo(Location2D.of(row, col));
      }
    }
  }
}