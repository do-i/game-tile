package com.djd.fun.gametile.model;

import com.djd.fun.gametile.service.Location2DService;
import com.djd.fun.gametile.service.Location2DServiceImpl;
import com.djd.fun.gametile.tiled.TiledDataResolver;
import com.djd.fun.gametile.tiled.model.TiledDataRequest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.djd.fun.gametile.model.Direction.EAST;
import static com.djd.fun.gametile.model.Direction.NORTH;
import static com.djd.fun.gametile.model.Direction.SOUTH;
import static com.djd.fun.gametile.model.Direction.WEST;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class CharacterStateTest extends Mockito {

  private Location2DService location2DService;
  private @Mock TiledDataResolver tiledDataResolver;

  @BeforeEach
  void setUp() {
    location2DService = new Location2DServiceImpl(Dimension.of(3, 3));
  }

  @Test
  void getLocation() {
    CharacterState characterState = new CharacterState(tiledDataResolver, location2DService,
        Location2D.of(0, 5));
    assertThat(characterState.getActiveLocation()).isEqualTo(Location2D.of(0, 5));
  }

  @Test
  void moveNorth() {
    Location2D destination = Location2D.of(0, 0);
    doReturn(true).when(tiledDataResolver).isAccessible(TiledDataRequest.builder().location(destination).build());
    CharacterState characterState = new CharacterState(tiledDataResolver, location2DService,
        Location2D.of(1, 0));
    characterState.moveTowards(NORTH);
    assertThat(characterState.getActiveLocation()).isEqualTo(destination);
    verifyNoMoreInteractions(tiledDataResolver);
  }

  @Test
  void moveNorth_noMove() {
    CharacterState characterState = new CharacterState(tiledDataResolver, location2DService,
        Location2D.of(0, 1));
    characterState.moveTowards(NORTH);
    assertThat(characterState.getActiveLocation()).isEqualTo(Location2D.of(0, 1));
  }

  @Test
  void moveWest() {
    Location2D destination = Location2D.of(2, 0);
    doReturn(true).when(tiledDataResolver).isAccessible(TiledDataRequest.builder().location(destination).build());
    CharacterState characterState = new CharacterState(tiledDataResolver, location2DService,
        Location2D.of(2, 1));
    characterState.moveTowards(WEST);
    assertThat(characterState.getActiveLocation()).isEqualTo(destination);
    verifyNoMoreInteractions(tiledDataResolver);
  }

  @Test
  void moveWest_noMove() {
    CharacterState characterState = new CharacterState(tiledDataResolver, location2DService,
        Location2D.of(1, 0));
    characterState.moveTowards(WEST);
    assertThat(characterState.getActiveLocation()).isEqualTo(Location2D.of(1, 0));
  }

  @Test
  void moveSouth() {
    Location2D destination = Location2D.of(2, 0);
    doReturn(true).when(tiledDataResolver).isAccessible(TiledDataRequest.builder().location(destination).build());
    CharacterState characterState = new CharacterState(tiledDataResolver, location2DService,
        Location2D.of(1, 0));
    characterState.moveTowards(SOUTH);
    assertThat(characterState.getActiveLocation()).isEqualTo(destination);
    verifyNoMoreInteractions(tiledDataResolver);
  }

  @Test
  void moveSouth_noMove() {
    CharacterState characterState = new CharacterState(tiledDataResolver, location2DService,
        Location2D.of(2, 0));
    characterState.moveTowards(SOUTH);
    assertThat(characterState.getActiveLocation()).isEqualTo(Location2D.of(2, 0));
  }

  @Test
  void moveEast() {
    Location2D initLocation = Location2D.of(0, 1);
    Location2D destination = Location2D.of(0, 2);
    doReturn(true).when(tiledDataResolver).isAccessible(TiledDataRequest.builder().location(destination).build());
    CharacterState characterState = new CharacterState(tiledDataResolver, location2DService,
        initLocation);
    characterState.moveTowards(EAST);
    assertThat(characterState.getActiveLocation()).isEqualTo(destination);
    verifyNoMoreInteractions(tiledDataResolver);
  }

  @Test
  void moveEast_noMove() {
    CharacterState characterState = new CharacterState(tiledDataResolver, location2DService,
        Location2D.of(0, 2));
    characterState.moveTowards(EAST);
    assertThat(characterState.getActiveLocation()).isEqualTo(Location2D.of(0, 2));
  }
}