package com.djd.fun.gametile.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class DimensionTest {

  @Test
  void of_tooShort() {
    assertThrows(IllegalArgumentException.class,
        () -> Dimension.of(1, 0));
  }

  @Test
  void of_tooNarrow() {
    assertThrows(IllegalArgumentException.class,
        () -> Dimension.of(0, 1));
  }

  @Test
  void of_tooSmall() {
    assertThrows(IllegalArgumentException.class,
        () -> Dimension.of(0, 0));
  }

}