package com.djd.fun.gametile.service;

import java.util.Optional;

import com.djd.fun.gametile.model.Dimension;
import com.djd.fun.gametile.model.Location2D;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static com.djd.fun.gametile.model.Direction.EAST;
import static com.djd.fun.gametile.model.Direction.NORTH;
import static com.djd.fun.gametile.model.Direction.SOUTH;
import static com.djd.fun.gametile.model.Direction.WEST;
import static com.google.common.truth.Truth.assertThat;

class Location2DServiceImplTest {

  private static final Location2D LOCATION_1_1 = Location2D.of(1, 1);
  private Location2DServiceImpl location2DService;

  @BeforeEach
  void setUp() {
    location2DService = new Location2DServiceImpl(Dimension.of(3, 3));
  }

  @Test
  void northOf() {
    assertThat(location2DService.northOf(Location2D.of(1, 1)).get())
        .isEqualTo(Location2D.of(0, 1));
  }

  @Test
  void eastOf() {
    assertThat(location2DService.eastOf(Location2D.of(1, 1)).get())
        .isEqualTo(Location2D.of(1, 2));
  }

  @Test
  void southOf() {
    assertThat(location2DService.southOf(Location2D.of(1, 1)).get())
        .isEqualTo(Location2D.of(2, 1));
  }

  @Test
  void westOf() {
    assertThat(location2DService.westOf(Location2D.of(1, 1)).get())
        .isEqualTo(Location2D.of(1, 0));
  }

  @Test
  void northOf_outOfBoundary() {
    assertThat(location2DService.northOf(Location2D.of(0, 1))).isSameAs(Optional.empty());
  }

  @Test
  void eastOf_outOfBoundary() {
    assertThat(location2DService.eastOf(Location2D.of(1, 2))).isSameAs(Optional.empty());
  }

  @Test
  void southOf_outOfBoundary() {
    assertThat(location2DService.southOf(Location2D.of(2, 1))).isSameAs(Optional.empty());
  }

  @Test
  void westOf_outOfBoundary() {
    assertThat(location2DService.westOf(Location2D.of(1, 0))).isSameAs(Optional.empty());
  }

  @ParameterizedTest
  @CsvSource({"0, -1", "-1, 0", "2, 3", "3, 2"})
  void checkBoundary(int row, int col) {
    assertThat(location2DService.checkBoundary(Location2D.of(row, col))).isSameAs(Optional.empty());
  }

  @Test
  void findDirection_north() {
    assertThat(location2DService.findDirection(LOCATION_1_1, Location2D.of(0, 1)).get())
        .isSameAs(NORTH);
  }

  @Test
  void findDirection_east() {
    assertThat(location2DService.findDirection(LOCATION_1_1, Location2D.of(1, 2)).get())
        .isSameAs(EAST);
  }

  @Test
  void findDirection_south() {
    assertThat(location2DService.findDirection(LOCATION_1_1, Location2D.of(2, 1)).get())
        .isSameAs(SOUTH);
  }

  @Test
  void findDirection_west() {
    assertThat(location2DService.findDirection(LOCATION_1_1, Location2D.of(1, 0)).get())
        .isSameAs(WEST);
  }
}