package com.djd.fun.gametile.service;

import com.djd.fun.gametile.model.Dimension;
import com.djd.fun.gametile.model.Location2D;
import com.djd.fun.gametile.tiled.TiledDataResolver;
import com.djd.fun.gametile.tiled.model.TiledDataRequest;
import com.google.common.collect.ImmutableSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class ManhattanMovementTest extends Mockito {

  private @Mock TiledDataResolver tiledDataResolver;
  private Location2DService location2DService;
  private ManhattanMovement manhattanMovement;

  @BeforeEach
  void setUp() {
    doReturn(true).when(tiledDataResolver).isAccessible(any(TiledDataRequest.class));
    location2DService = new Location2DServiceImpl(Dimension.of(3, 3));
    manhattanMovement = new ManhattanMovement(location2DService, tiledDataResolver);
  }

  /**
   * [S][ ][ ]
   * [ ][O][ ]
   * [ ][ ][D]
   * <p>
   * S: start, O: obstacle, D: destination
   */
  @Test
  void computeNextMove_southOrEast() {
    Location2D start = Location2D.of(0, 0);
    Location2D destination = Location2D.of(2, 2);
    ImmutableSet<Location2D> obstacles = ImmutableSet.of(Location2D.of(1, 1));
    assertThat(manhattanMovement.computeNextMove(start, destination, obstacles))
        .isAnyOf(Location2D.of(0, 1), Location2D.of(1, 0));
  }

  /**
   * [ ][ ][D]
   * [S][O][ ]
   * [ ][ ][ ]
   * <p>
   * S: start, O: obstacle, D: destination
   */
  @Test
  void computeNextMove_north() {
    Location2D start = Location2D.of(1, 0);
    Location2D destination = Location2D.of(0, 2);
    ImmutableSet<Location2D> obstacles = ImmutableSet.of(Location2D.of(1, 1));
    assertThat(manhattanMovement.computeNextMove(start, destination, obstacles))
        .isEqualTo(Location2D.of(0, 0));
  }

  /**
   * [ ][O][D]
   * [S][ ][ ]
   * [ ][ ][ ]
   * <p>
   * S: start, O: obstacle, D: destination
   */
  @Test
  void computeNextMove_northOrEast() {
    Location2D start = Location2D.of(1, 0);
    Location2D destination = Location2D.of(0, 2);
    ImmutableSet<Location2D> obstacles = ImmutableSet.of(Location2D.of(0, 1));
    assertThat(manhattanMovement.computeNextMove(start, destination, obstacles))
        .isAnyOf(Location2D.of(0, 0), Location2D.of(1, 1));
  }
}