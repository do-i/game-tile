package com.djd.fun.gametile.tiled;

import java.awt.image.BufferedImage;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.djd.fun.gametile.model.Location2D;
import com.djd.fun.gametile.tiled.model.TiledData;
import com.djd.fun.gametile.tiled.model.TiledDataRequest;
import com.djd.fun.gametile.tiled.model.TiledGroup;
import com.djd.fun.gametile.tiled.model.TiledLayer;
import com.djd.fun.gametile.tiled.model.TiledMapData;
import com.djd.fun.gametile.tiled.model.sprite.BufferedSprites;
import com.google.common.collect.ImmutableList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.djd.fun.gametile.tiled.model.TiledDataRequest.LAYER_1_ACCESSIBLE_BG;
import static com.djd.fun.gametile.tiled.model.TiledDataRequest.LAYER_2_INACCESSIBLE_BG;
import static com.djd.fun.gametile.tiled.model.TiledDataRequest.LAYER_4_INACCESSIBLE_FG;
import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class TiledDataResolverTest extends Mockito {

  public static final Location2D LOCATION = Location2D.of(2, 3);
  private @Mock TiledMapData tiledMapData;
  private @Mock BufferedSprites bufferedSprites;
  private @Mock TiledGroup tiledGroup;
  private @Mock TiledLayer tiledLayer1;
  private @Mock TiledLayer tiledLayer2;
  private @Mock TiledLayer tiledLayer3;
  private @Mock TiledLayer tiledLayer4;
  private @Mock TiledData tiledData;
  private @Mock BufferedImage sprite;
  private ImmutableList<TiledGroup> tiledGroups;
  private ImmutableList<TiledLayer> tiledLayers;
  private TiledDataResolver tiledDataResolver;

  @BeforeEach
  void setUp() {

    tiledGroups = ImmutableList.of(tiledGroup);
    tiledLayers = ImmutableList.of(tiledLayer1, tiledLayer2, tiledLayer3, tiledLayer4);
    doReturn(tiledGroups).when(tiledMapData).getTiledGroups();
    tiledDataResolver = new TiledDataResolver(tiledMapData, bufferedSprites);
  }

  @Test
  void getMapWidth() {
    doReturn(18).when(tiledMapData).getWidth();
    assertThat(tiledDataResolver.getMapWidth()).isEqualTo(18);
  }

  @Test
  void getMapHeight() {
    doReturn(28).when(tiledMapData).getHeight();
    assertThat(tiledDataResolver.getMapHeight()).isEqualTo(28);
  }

  @Test
  void getSprite() {
    doReturn(tiledLayer1).when(tiledGroup).getTiledLayer(LAYER_1_ACCESSIBLE_BG);
    doReturn(tiledData).when(tiledLayer1).getTiledData();
    doReturn(1).when(tiledData).getGlobalIdAt(LOCATION);
    doReturn(sprite).when(bufferedSprites).getSprite(1);
    assertThat(tiledDataResolver.getSprite(TiledDataRequest.with(1, LAYER_1_ACCESSIBLE_BG, LOCATION)).get())
        .isEqualTo(sprite);
  }

  @Test
  void getSprite_empty() {
    doReturn(tiledLayer1).when(tiledGroup).getTiledLayer(LAYER_1_ACCESSIBLE_BG);
    doReturn(tiledData).when(tiledLayer1).getTiledData();
    doReturn(0).when(tiledData).getGlobalIdAt(LOCATION);
    assertThat(tiledDataResolver.getSprite(TiledDataRequest.with(1, LAYER_1_ACCESSIBLE_BG, LOCATION)))
        .isSameAs(Optional.empty());
  }

  @Test
  void getSpriteAt() {
    doReturn(sprite).when(bufferedSprites).getSprite(1);
    assertThat(tiledDataResolver.getSpriteAt(1)).isEqualTo(sprite);
  }

  @Test
  void getSpriteAt_none() {
    doThrow(NoSuchElementException.class).when(bufferedSprites).getSprite(0);
    assertThrows(NoSuchElementException.class, () -> tiledDataResolver.getSpriteAt(0));
  }

  @Test
  void isAccessible_true() {
    doReturn(tiledLayer2).when(tiledGroup).getTiledLayer(LAYER_2_INACCESSIBLE_BG);
    doReturn(tiledLayer4).when(tiledGroup).getTiledLayer(LAYER_4_INACCESSIBLE_FG);
    doReturn(tiledData).when(tiledLayer2).getTiledData();
    doReturn(tiledData).when(tiledLayer4).getTiledData();
    doReturn(0).when(tiledData).getGlobalIdAt(LOCATION);
    assertThat(tiledDataResolver.isAccessible(TiledDataRequest.builder().location(LOCATION).build())).isTrue();
  }

  @Test
  void isAccessible_layer4_false() {
    doReturn(tiledLayer2).when(tiledGroup).getTiledLayer(LAYER_2_INACCESSIBLE_BG);
    doReturn(tiledLayer4).when(tiledGroup).getTiledLayer(LAYER_4_INACCESSIBLE_FG);
    doReturn(tiledData).when(tiledLayer2).getTiledData();
    doReturn(tiledData).when(tiledLayer4).getTiledData();
    doReturn(0).doReturn(69).when(tiledData).getGlobalIdAt(LOCATION);
    assertThat(tiledDataResolver.isAccessible(TiledDataRequest.builder().location(LOCATION).build())).isFalse();
  }

  @Test
  void isAccessible_layer2_false() {
    doReturn(tiledLayer2).when(tiledGroup).getTiledLayer(LAYER_2_INACCESSIBLE_BG);
    doReturn(tiledData).when(tiledLayer2).getTiledData();
    doReturn(69).when(tiledData).getGlobalIdAt(LOCATION);
    assertThat(tiledDataResolver.isAccessible(TiledDataRequest.builder().location(LOCATION).build())).isFalse();
  }

  @Test
  void getTiledData() {
    doReturn(tiledLayer1).when(tiledGroup).getTiledLayer(LAYER_1_ACCESSIBLE_BG);
    doReturn(tiledData).when(tiledLayer1).getTiledData();
    assertThat(tiledDataResolver.getTiledData(TiledDataRequest.with(1, LAYER_1_ACCESSIBLE_BG, LOCATION)))
        .isSameAs(tiledData);
  }
}