package com.djd.fun.gametile.tiled.model;

import com.djd.fun.gametile.model.Location2D;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static com.google.common.truth.Truth.assertThat;

class TiledDataTest {

  private static final String data = "1,2,3,\n4,5,6,\n7,8,9,\n10,11,12\n";
  private TiledData tiledData;

  @BeforeEach
  void setUp() {
    tiledData = new TiledData(data, 3, 4);
  }

  @ParameterizedTest
  @CsvSource({
      "0,0,1", "0,1,2", "0,2,3", "0,3,4",
      "1,0,5", "1,1,6", "1,2,7", "1,3,8",
      "2,0,9", "2,1,10", "2,2,11", "2,3,12"})
  void getGlobalIdAt(int row, int col, int gid) {
    assertThat(tiledData.getGlobalIdAt(Location2D.of(row, col))).isEqualTo(gid);
  }
}