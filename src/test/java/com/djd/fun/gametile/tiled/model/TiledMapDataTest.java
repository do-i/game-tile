package com.djd.fun.gametile.tiled.model;

import java.io.IOException;
import java.net.URL;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.google.common.io.Resources;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

class TiledMapDataTest {

  private ObjectMapper objectMapper;

  @BeforeEach
  void setUp() {
    objectMapper = new XmlMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    objectMapper.registerModule(new GuavaModule());
    objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
  }

  @Test
  void deserialize() throws IOException {
    URL tiledDataFileURL = Resources.getResource("layers_level_001.tmx");
    TiledMapData tiledMapData = objectMapper.readValue(tiledDataFileURL, TiledMapData.class);
    assertThat(tiledMapData.getHeight()).isEqualTo(20);
    assertThat(tiledMapData.getWidth()).isEqualTo(20);
    assertThat(tiledMapData.getTiledTileSets()).hasSize(2);
    assertThat(tiledMapData.getTiledGroups()).hasSize(1);
    TiledGroup level1TiledGroup = tiledMapData.getTiledGroups().get(0);
    TiledDataRequest.getTerrainLayerNames().forEach(level1TiledGroup::getTiledLayer);
  }
}