package poc.prim;

import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LocationChainTest {

  @Test
  void createOppositeFromParent() {
    LocationChain location = LocationChain.of(1, 1,
        LocationChain.of(1, 1));
    assertThrows(IllegalStateException.class, () -> location.createOppositeFromParent());
  }

  @Test
  void createOppositeFromParent_sameRowLowParent_sameRowHighNew() {
    LocationChain location = LocationChain.of(1, 1,
        LocationChain.of(1, 0));
    assertThat(location.createOppositeFromParent())
        .isEqualTo(LocationChain.of(1, 2));
  }

  @Test
  void createOppositeFromParent_sameRowHighParent_sameRowLowNew() {
    LocationChain location = LocationChain.of(1, 1,
        LocationChain.of(1, 2));
    assertThat(location.createOppositeFromParent())
        .isEqualTo(LocationChain.of(1, 0));
  }

  @Test
  void createOppositeFromParent_sameColLowParent_sameColHighNew() {
    LocationChain location = LocationChain.of(1, 1,
        LocationChain.of(0, 1));
    assertThat(location.createOppositeFromParent())
        .isEqualTo(LocationChain.of(2, 1));
  }

  @Test
  void createOppositeFromParent_sameColHighParent_sameColLowNew() {
    LocationChain location = LocationChain.of(1, 1,
        LocationChain.of(2, 1));
    assertThat(location.createOppositeFromParent())
        .isEqualTo(LocationChain.of(0, 1));
  }
}