package poc.prim;

import com.google.common.collect.ImmutableList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static poc.prim.MutableMatrix.FloorState.FLOOR;

class MutableMatrixTest {

  private MutableMatrix matrix;

  @BeforeEach
  void setUp() {
    matrix = new MutableMatrix(new Dimension(3, 3));
  }

  /**
   * C: current location
   * # N #
   * W C E
   * # S #
   */
  @Test
  void findAdjacentWalls_allWall_NESW() {
    ImmutableList<LocationChain> adjacents = matrix.findAdjacentWalls(
        LocationChain.of(1, 1));
    assertThat(adjacents).containsExactly(
        LocationChain.of(0, 1),
        LocationChain.of(1, 2),
        LocationChain.of(2, 1),
        LocationChain.of(1, 0));
  }

  /**
   * C: current location
   * <p>
   * W C E
   * # S #
   * # # #
   */
  @Test
  void findAdjacentWalls_allWall_ESW() {
    ImmutableList<LocationChain> adjacents = matrix.findAdjacentWalls(
        LocationChain.of(0, 1));
    assertThat(adjacents).containsExactly(
        LocationChain.of(0, 2),
        LocationChain.of(1, 1),
        LocationChain.of(0, 0));
  }

  /**
   * C: current location
   * # # N
   * # W C
   * # # S
   */
  @Test
  void findAdjacentWalls_allWall_NSW() {
    ImmutableList<LocationChain> adjacents = matrix.findAdjacentWalls(
        LocationChain.of(1, 2));
    assertThat(adjacents).containsExactly(
        LocationChain.of(0, 2),
        LocationChain.of(2, 2),
        LocationChain.of(1, 1));
  }

  /**
   * C: current location
   * # # #
   * # N #
   * W C E
   */
  @Test
  void findAdjacentWalls_allWall_NEW() {
    ImmutableList<LocationChain> adjacents = matrix.findAdjacentWalls(
        LocationChain.of(2, 1));
    assertThat(adjacents).containsExactly(
        LocationChain.of(1, 1),
        LocationChain.of(2, 2),
        LocationChain.of(2, 0));
  }

  /**
   * C: current location
   * N # #
   * C E #
   * S # #
   */
  @Test
  void findAdjacentWalls_allWall_NES() {
    ImmutableList<LocationChain> adjacents = matrix.findAdjacentWalls(
        LocationChain.of(1, 0));
    assertThat(adjacents).containsExactly(
        LocationChain.of(0, 0),
        LocationChain.of(1, 1),
        LocationChain.of(2, 0));
  }

  /**
   * C: current location
   * C E #
   * S # #
   * # # #
   */
  @Test
  void findAdjacentWalls_allWall_ES() {
    ImmutableList<LocationChain> adjacents = matrix.findAdjacentWalls(
        LocationChain.of(0, 0));
    assertThat(adjacents).containsExactly(
        LocationChain.of(0, 1),
        LocationChain.of(1, 0));
  }

  /**
   * C: current location
   * # W C
   * # # S
   * # # #
   */
  @Test
  void findAdjacentWalls_allWall_SW() {
    ImmutableList<LocationChain> adjacents = matrix.findAdjacentWalls(
        LocationChain.of(0, 2));
    assertThat(adjacents).containsExactly(
        LocationChain.of(0, 1),
        LocationChain.of(1, 2));
  }

  /**
   * C: current location
   * # # #
   * # # N
   * # W C
   */
  @Test
  void findAdjacentWalls_allWall_NW() {
    ImmutableList<LocationChain> adjacents = matrix.findAdjacentWalls(
        LocationChain.of(2, 2));
    assertThat(adjacents).containsExactly(
        LocationChain.of(1, 2),
        LocationChain.of(2, 1));
  }

  /**
   * C: current location
   * # # #
   * N # #
   * C E #
   */
  @Test
  void findAdjacentWalls_allWall_NE() {
    ImmutableList<LocationChain> adjacents = matrix.findAdjacentWalls(
        LocationChain.of(2, 0));
    assertThat(adjacents).containsExactly(
        LocationChain.of(1, 0),
        LocationChain.of(2, 1));
  }

  /**
   * C: current location
   * # # #
   * W C E
   * # # #
   */
  @Test
  void findAdjacentWalls_nonWall_NESW() {
    matrix.changeStateAtLocation(FLOOR, LocationChain.of(0, 1));
    matrix.changeStateAtLocation(FLOOR, LocationChain.of(2, 1));
    ImmutableList<LocationChain> adjacents = matrix.findAdjacentWalls(
        LocationChain.of(1, 1));
    assertThat(adjacents).containsExactly(
        LocationChain.of(1, 2),
        LocationChain.of(1, 0));
  }
}